const styles = theme => ({
  '@global': {
    html: {
      height: '100%',
      margin: 0,
    },
    body: {
      height: '100%',
      backgroundColor: '#eee',
    },
    '.text-center': {
      textAlign: 'center',
    },
    '.material-icons': {
      '&.pointer': {
        cursor: 'pointer',
      },
      '&.inner-icons': {
        marginRight: '5px',
        fontSize: '20px',
      },
      '&.icon-md': {
        fontSize: '28px',
      },
    },
    '.m-r-xs': {
      marginRight: '5px',
    },
    '.m-r-lg': {
      marginRight: '25px',
    },
    '.m-l': {
      marginLeft: '15px',
    },
    '.m-b': {
      marginBottom: theme.spacing.unit,
    },
    '.p-sides': {
      padding: '0 10px',
    },
    a: {
      color: 'inherit',
      textDecoration: 'none',
      '&:hover,&:active,&:visited': {
        textDecoration: 'none',
      },
    },
    '.btnOutlined': {
      border: '1px solid #d9e0538f',
    },
    '.table-md': {
      minWidth: '700px',
    },
    '.sorted': {
      position: 'relative',
      cursor: 'pointer',
      '&.asc': {
        '&::after': {
          content: '"↑"',
          color: '#000',
          position: 'absolute',
          top: '30%',
          right: '2%',
        },
      },
      '&.desc': {
        '&::after': {
          content: '"↓"',
          color: '#000',
          position: 'absolute',
          top: '30%',
          right: '2%',
        },
      },
    },
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    minHeight: 'calc(100vh - 64px)',
    marginBottom: '-40px',
    paddingBottom: '40px',
    padding: '20px 0 0 0',
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    [theme.breakpoints.down('xs')]: {
      marginRight: theme.spacing.unit * 1.5,
      marginLeft: theme.spacing.unit * 1.5,
    },
  },
});

export default styles;
