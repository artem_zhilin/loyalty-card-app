const styles = theme => ({
  toolbarTitle: {
    marginLeft: theme.spacing.unit * 1.5,
    marginRight: theme.spacing.unit * 2.5,
    display: 'inline-block',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  buttonWrap: {
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
  logoIcon: {
    fontSize: '30px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '24px',
    },
  },
  btnText: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  btnSignOut: {
    marginLeft: '20px',
    [theme.breakpoints.down('xs')]: {
      marginLeft: '5px',
    },
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
});

export default styles;
