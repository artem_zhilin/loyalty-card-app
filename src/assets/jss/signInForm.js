const styles = theme => ({
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  signInTitle: {
    fontSize: '1.45em',
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.15em',
    },
  },
  form: {
    width: '50%',
    marginTop: theme.spacing.unit,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  error: {
    color: theme.palette.error.main,
    fontSize: '1.1em',
    margin: '20px 0 0 0',
  },
});

export default styles;
