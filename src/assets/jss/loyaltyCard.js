const styles = theme => ({
  root: {
    position: 'relative',
    width: '300px',
    height: '190px',
    margin: '10px auto',
    padding: '10px',
    borderRadius: '12px',
    boxShadow: '0px 6px 10px 0px rgba(128,222,234,0.7)',
    background: 'linear-gradient(0deg, #0097A7 0%, #00ACC1 30%, #006064bf 100%)',
    color: '#fff',
    [theme.breakpoints.down('md')]: {
      width: '280px',
      height: '185px',
    },
    [theme.breakpoints.down('sm')]: {
      width: '275px',
      height: '180px',
    },
    [theme.breakpoints.down('xs')]: {
      width: '265px',
      height: '170px',
    },
  },
  title: {
    fontSize: '1.1em',
  },
  expired: {
    position: 'absolute',
    bottom: '2%',
    right: '2%',
    fontSize: '0.9em',
  },
  provider: {
    position: 'absolute',
    bottom: '2%',
    left: '2%',
    fontSize: '0.9em',
  },
  type: {
    fontSize: '1em',
    fontWeight: '400',
  },
  achievedReward: {
    fontSize: '1em',
    fontWeight: '400',
  },
  nextReward: {
    fontSize: '1em',
    marginTop: '5px',
  },
  ownerButtons: {
    position: 'absolute',
    top: '70%',
    right: '2%',
  },
});

export default styles;
