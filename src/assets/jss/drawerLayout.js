const styles = theme => ({
  '@global': {
    html: {
      height: '100%',
      margin: 0,
    },
    body: {
      height: '100%',
      backgroundColor: '#eee',
    },
    '#root': {
      height: '100%',
    },
    '.text-center': {
      textAlign: 'center',
    },
    '.block': {
      display: 'block',
    },
    '.material-icons': {
      '&.pointer': {
        cursor: 'pointer',
      },
      '&.inner-icons': {
        marginRight: '5px',
        fontSize: '20px',
      },
      '&.icon-md': {
        fontSize: '28px',
      },
    },
    '.m-r-xs': {
      marginRight: '5px',
    },
    '.m-r-lg': {
      marginRight: '25px',
    },
    '.m-l': {
      marginLeft: '15px',
    },
    '.m-l-sm': {
      marginLeft: '10px',
    },
    '.m-b': {
      marginBottom: '10px',
    },
    '.p-sides': {
      padding: '0 10px',
    },
    a: {
      color: 'inherit',
      textDecoration: 'none',
      '&:hover,&:active,&:visited': {
        textDecoration: 'none',
      },
    },
    '.btnOutlined': {
      border: '1px solid #d9e0538f',
    },
    '.table-md': {
      minWidth: '700px',
      [theme.breakpoints.down('xs')]: {
        minWidth: '500px',
      },
    },
    '.sorted': {
      position: 'relative',
      cursor: 'pointer',
      '&.asc': {
        '&::after': {
          content: '"↑"',
          color: '#000',
          position: 'absolute',
          top: '30%',
          right: '2%',
        },
      },
      '&.desc': {
        '&::after': {
          content: '"↓"',
          color: '#000',
          position: 'absolute',
          top: '30%',
          right: '2%',
        },
      },
    },
  },
  root: {
    display: 'flex',
    minHeight: 'calc(100vh - 64px)',
    marginBottom: '-40px',
    paddingBottom: '40px',
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
});

export default styles;
