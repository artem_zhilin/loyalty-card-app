const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: '#d9eae9',
    },
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  footer: {
    marginTop: '48px',
    borderTop: '1px solid #010101',
    padding: '36px 0',
  },
});

export default styles;
