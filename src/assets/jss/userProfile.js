const styles = theme => ({
  formTitle: {
    color: '#fff',
    margin: '0',
    fontSize: '1.3em',
    fontWeight: '400',
    marginTop: '0',
    marginBottom: '0',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    [theme.breakpoints.down('xs')]: {
      fontSize: '1em',
    },
  },
  formDescription: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: theme.spacing.unit * 0.5,
    textDecoration: 'none',
  },
  pdsm: {
    padding: theme.spacing.unit * 2.5,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing.unit * 0.5,
    },
  },
  contentCenterXs: {
    justifyContent: 'center',
  },
  avatarWrapper: {
    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
  avatarImg: {
    height: 'auto',
    width: '100%',
    borderRadius: '50%',
    [theme.breakpoints.down('xs')]: {
      width: '30%',
    },
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: '20px',
  },
});

export default styles;
