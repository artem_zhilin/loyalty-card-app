const styles = theme => ({
  footer: {
    backgroundColor: theme.palette.primary.main,
    height: '40px',
    color: '#fff',
    overflow: 'hidden',
    zIndex: theme.zIndex.drawer + 1,
  },
  ptxs: {
    paddingTop: '5px',
  },
});

export default styles;
