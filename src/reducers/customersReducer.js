import {
  FETCH_CUSTOMER, FETCH_CUSTOMERS, SORT_CUSTOMERS, FILTER_CUSTOMERS,
} from '../types/customers';

const initialState = {
  initialCustomers: [],
  customers: [],
  customer: {},
  isSortAborted: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CUSTOMERS:
      return {
        ...state,
        initialCustomers: action.payload,
        customers: action.payload,
      };
    case SORT_CUSTOMERS:
      return {
        ...state,
        customers: action.payload,
        isSortAborted: false,
      };
    case FETCH_CUSTOMER:
      return {
        ...state,
        customer: action.payload,
      };
    case FILTER_CUSTOMERS:
      return {
        ...state,
        customers: action.payload,
        isSortAborted: true,
      };
    default:
      return state;
  }
};
