/* eslint-disable no-undef */
import moment from 'moment';
import {
  FETCH_CLIENT_CARDS,
  SORT_CARDS,
  FILTER_CARDS,
  REMOVE_CARD,
  FETCH_ALL_CARDS,
  ADD_CARD,
  EDIT_CARD,
  FETCH_CARD,
  FETCH_CUSTOMER_CARDS,
} from '../types/cards';

const initialState = {
  clientCards: [],
  customerCards: [],
  initialCards: JSON.parse(localStorage.getItem('initialCards'))
    ? JSON.parse(localStorage.getItem('initialCards'), (key, value) => (key === 'created_at' || key === 'expired_at' ? moment(value) : value)) : [],
  card: {
    rewards: [],
  },
  isSortAborted: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_CARDS:
      return {
        ...state,
        initialCards: action.payload,
      };
    case FETCH_CLIENT_CARDS:
      return {
        ...state,
        clientCards: action.payload,
      };
    case FETCH_CUSTOMER_CARDS:
      return {
        ...state,
        customerCards: action.payload,
      };
    case SORT_CARDS:
      return {
        ...state,
        clientCards: action.payload,
        isSortAborted: false,
      };
    case FILTER_CARDS:
      return {
        ...state,
        clientCards: action.payload,
        isSortAborted: true,
      };
    case ADD_CARD:
      return {
        ...state,
        initialCards: [...state.initialCards, action.payload],
      };
    case FETCH_CARD:
      return {
        ...state,
        card: action.payload,
      };
    case EDIT_CARD: {
      const updatedCard = { ...state.card, ...action.payload };
      return {
        ...state,
        initialCards: [
          ...state.initialCards.filter(c => c.id !== state.card.id),
          updatedCard,
        ],
        card: updatedCard,
      };
    }
    case REMOVE_CARD:
      return {
        ...state,
        initialCards: state.initialCards.filter(c => c.id !== action.payload),
        customerCards: state.customerCards.filter(c => c.id !== action.payload),
      };
    default:
      return state;
  }
};
