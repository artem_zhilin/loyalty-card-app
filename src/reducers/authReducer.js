/* eslint-disable no-undef */
import { DO_AUTH, DO_LOGOUT, UPDATE_USER } from '../types/auth';

const initialState = {
  token: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).token : '',
  isAuthenticated: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).isAuthenticated : false,
  user: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).user : {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case DO_AUTH: {
      return {
        ...state,
        user: action.payload,
        token: btoa(JSON.stringify(action.payload)),
        isAuthenticated: true,
      };
    }
    case UPDATE_USER: {
      const updatedUser = { ...state.user, ...action.payload };
      return {
        ...state,
        user: updatedUser,
        token: btoa(JSON.stringify(updatedUser)),
      };
    }
    case DO_LOGOUT: {
      localStorage.removeItem('auth');
      return {
        token: '', user: {}, isAuthenticated: false,
      };
    }
    default:
      return state;
  }
}
