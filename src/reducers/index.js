import { combineReducers } from 'redux';
import authReducer from './authReducer';
import cardsReducer from './cardsReducer';
import customersReducer from './customersReducer';
import purchasesReducer from './purchasesReducer';

export default combineReducers({
  auth: authReducer,
  cards: cardsReducer,
  customers: customersReducer,
  purchases: purchasesReducer,
});
