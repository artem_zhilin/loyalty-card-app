import {
    FETCH_CLIENT_PURCHASES,
    SORT_CLIENT_PURCHASES,
    FILTER_CLIENT_PURCHASES,
    FETCH_CUSTOMERS_PURCHASES,
    FETCH_CUSTOMERS_STATISTICS,
    SORT_CUSTOMERS_PURCHASES,
    FILTER_CUSTOMERS_PURCHASES
} from '../types/purchases';

const initialState = {
  clientPurchases: [],
  clientFilteredPurchases: [],
  customersPurchases: [],
  customersFilteredPurchases: [],
  customersStatistics: { customersCount: 0, cardsCount: 0, purchasesCount: 0 },
  isSortAborted: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CLIENT_PURCHASES:
      return {
        ...state,
        clientPurchases: action.payload,
        clientFilteredPurchases: action.payload,
      };
    case SORT_CLIENT_PURCHASES:
      return {
        ...state,
        clientFilteredPurchases: action.payload,
        isSortAborted: false,
      };
    case FILTER_CLIENT_PURCHASES:
      return {
        ...state,
        clientFilteredPurchases: action.payload,
        isSortAborted: true,
      };
    case FETCH_CUSTOMERS_STATISTICS:
      return {
        ...state,
        customersStatistics: action.payload,
      };
    case FETCH_CUSTOMERS_PURCHASES:
      return {
        ...state,
        customersPurchases: action.payload,
        customersFilteredPurchases: action.payload,
      };
    case SORT_CUSTOMERS_PURCHASES:
      return {
        ...state,
        customersFilteredPurchases: action.payload,
        isSortAborted: false,
      };
    case FILTER_CUSTOMERS_PURCHASES:
      return {
        ...state,
        customersFilteredPurchases: action.payload,
        isSortAborted: true,
      };
    default:
      return state;
  }
};
