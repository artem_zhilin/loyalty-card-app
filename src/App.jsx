import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import store from './store';
import PrivateRoute from './routes/PrivateRoute';
import SignInForm from './pages/SignInForm';
import {
  SIGN_IN,
  PrivateRoutes,
} from './routes';
import EmptyLayout from './layout/EmptyLayout';
import PublicRoute from './routes/PublicRoute';
import NotFound from './pages/NotFound';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        {PrivateRoutes.map(route => (
          <PrivateRoute
            key={route.path}
            layout={route.layout}
            forOwner={route.forOwner}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
        <PublicRoute layout={EmptyLayout} path={SIGN_IN} exact component={SignInForm} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  </Provider>
);

export default App;
