/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { isEmpty } from 'lodash';
import rootReducer from './reducers';

const initialState = {};

const middleware = [thunk];

const composeEnhancers = typeof window === 'object'
  && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
  }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
);

const store = createStore(rootReducer, initialState, enhancer);

store.subscribe(() => {
  const { auth, cards } = store.getState();
  if (auth.isAuthenticated) {
    localStorage.setItem('auth', JSON.stringify(auth));
  } else {
    localStorage.removeItem('auth');
  }
  if (!isEmpty(cards.initialCards)) {
    localStorage.setItem('initialCards', JSON.stringify(cards.initialCards));
  }
});

export default store;
