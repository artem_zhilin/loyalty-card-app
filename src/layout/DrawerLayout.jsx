import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import DrawerStyled from '../components/Drawer/DrawerStyled';
import styles from '../assets/jss/drawerLayout';
import Navbar from '../components/Navbar/Navbar';
import { getIsOwner } from '../selectors/auth';
import { logout } from '../actions/authActions';
import Footer from '../components/Footer/Footer';

const DrawerLayout = ({
  classes, children, logoutDispatch, isOwner,
}) => {
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <Fragment>
      <div className={classes.root}>
        <CssBaseline />
        <Navbar logoutDispatch={logoutDispatch} handleDrawerToggle={handleDrawerToggle} />
        <DrawerStyled
          isOwner={isOwner}
          mobileOpen={mobileOpen}
          handleDrawerToggle={handleDrawerToggle}
        />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
      <Footer />
    </Fragment>
  );
};

DrawerLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  isOwner: PropTypes.bool.isRequired,
  logoutDispatch: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const mapStateToProps = state => ({
  isOwner: getIsOwner(state),
});

const mapDispatchToProps = {
  logoutDispatch: logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DrawerLayout));
