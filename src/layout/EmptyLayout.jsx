import React, { Fragment } from 'react';
import { CssBaseline, withStyles } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import styles from '../assets/jss/emptyLayout';

const EmptyLayout = ({ children, classes }) => (
  <Fragment>
    <CssBaseline />
    <main className={classes.layout}>
      {children}
    </main>
  </Fragment>
);

EmptyLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,

};

export default withStyles(styles)(EmptyLayout);
