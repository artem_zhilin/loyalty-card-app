import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import Navbar from '../components/Navbar/Navbar';
import styles from '../assets/jss/mainLayout';
import Footer from '../components/Footer/Footer';

const MainLayout = ({ classes, children }) => (
  <Fragment>
    <CssBaseline />
    <Navbar />
    <main className={classes.layout}>
      {children}
    </main>
    <Footer />
  </Fragment>
);

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default withStyles(styles)(MainLayout);
