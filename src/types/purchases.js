export const FETCH_CLIENT_PURCHASES = 'FETCH_CLIENT_PURCHASES';
export const SORT_CLIENT_PURCHASES = 'SORT_CLIENT_PURCHASES';
export const FILTER_CLIENT_PURCHASES = 'FILTER_CLIENT_PURCHASES';
export const FETCH_CUSTOMERS_STATISTICS = 'FETCH_CUSTOMERS_STATISTICS';
export const FETCH_CUSTOMERS_PURCHASES = 'FETCH_CUSTOMERS_PURCHASES';
export const SORT_CUSTOMERS_PURCHASES = 'SORT_CUSTOMERS_PURCHASES';
export const FILTER_CUSTOMERS_PURCHASES = 'FILTER_CUSTOMERS_PURCHASES';
