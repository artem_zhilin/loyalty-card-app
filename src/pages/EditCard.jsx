import React, { useState, useEffect } from 'react';
import {
  FormControl,
  Grid,
  Input,
  InputLabel,
  Button,
  Typography,
  Select,
} from '@material-ui/core';
import MomentUtils from '@date-io/moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { InlineDatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import moment from 'moment';
import { Link } from 'react-router-dom';
import PaperHeader from '../components/Paper/PaperHeader';
import PaperView from '../components/Paper/PaperView';
import { fetchCustomers } from '../actions/customersActions';
import LoyaltyCard from '../components/LoyaltyCards/LoyaltyCard';
import {
  editCard, fetchCard, editReward, addReward, removeReward,
} from '../actions/cardsActions';
import { CUSTOMER_CARDS } from '../routes';
import MenuItemStyled from '../components/MenuItem/MenuItemStyled';
import FormStyled from '../components/Form/FormStyled';
import FormGroupStyled from '../components/Form/FormGroupStyled';
import RewardEditable from '../components/LoyaltyCards/RewardEditable';
import SnackbarStyled from '../components/Snackbar/SnackbarStyled';
import ButtonOutlined from '../components/Button/ButtonOutlined';
import { getCard } from '../selectors/cards';
import { getAuthenticatedUser } from '../selectors/auth';
import { getCustomers } from '../selectors/customers';
import { USER_SHAPE, CARD_SHAPE } from '../constants';

const EditCard = ({
  fetchCustomersDispatch,
  editRewardDispatch,
  addRewardDispatch,
  editCardDispatch,
  removeRewardDispatch,
  customers,
  owner,
  match: { params },
  card,
  fetchCardDispatch,
}) => {
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [addRewardOpened, setAddRewardOpened] = useState(false);
  const [cardData, setCardData] = useState({
    title: '',
    provider: '',
    type: '',
    currency: '',
    amount: 0,
    expired_at: moment(),
    rewards: [],
  });

  const [cardHolder, setCardHolder] = useState([]);
  const [providersList, setProvidersList] = useState([]);

  useEffect(() => {
    const cardId = params.id;
    if (isEmpty(customers)) {
      fetchCustomersDispatch();
    } else if (isEmpty(card) || card.id !== cardId) {
      fetchCardDispatch(cardId);
    } else {
      const customer = customers.find(c => c.id === card.user_id);
      const newCardData = Object.keys(cardData)
        .reduce((accumulator, key) => {
          accumulator[key] = card[key];
          return accumulator;
        }, {});
      setCardData(newCardData);
      setCardHolder(customer ? customer.firstName : 'Unknown');
      setProvidersList(owner.applications.map(a => a.title));
    }
  }, [customers, card]);

  const updateCard = (e) => {
    e.preventDefault();
    editCardDispatch(cardData).then(() => setSnackbarOpened(true));
  };

  const handleInputChange = ({ target: { name, value } }) => {
    setCardData({ ...cardData, [name]: value });
  };

  const handleExpireChange = (value) => {
    setCardData({ ...cardData, expired_at: value });
  };

  const showAddReward = () => {
    setAddRewardOpened(true);
  };

  const updateReward = (rewardData) => {
    editRewardDispatch(rewardData);
    setSnackbarOpened(true);
  };

  const createReward = (rewardData) => {
    addRewardDispatch(rewardData);
    setSnackbarOpened(true);
    setAddRewardOpened(false);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpened(false);
  };

  const typesList = [
    'Savings',
  ];

  const currenciesList = [
    'Dollars',
    'Stars',
    'Cats',
    'Balls',
  ];

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Grid container justify="center" alignItems="center">
        <Grid item xs={12} sm={12} md={11}>
          <PaperHeader>
            <Grid container direction="row" justify="space-between">
              <Typography color="inherit" variant="h5" inline>Edit card</Typography>
              <Link to={CUSTOMER_CARDS.replace(':id', card.user_id)}>
                <ButtonOutlined>
                  <i className="material-icons inner-icons">arrow_back</i>
                    Back
                </ButtonOutlined>
              </Link>
            </Grid>
          </PaperHeader>
          <PaperView>
            <Grid container direction="row">
              <Grid item xs={12} sm={12} md={7} lg={7}>
                <FormStyled onSubmit={updateCard}>
                  <FormGroupStyled>
                    <FormControl disabled margin="normal" required fullWidth>
                      <InputLabel htmlFor="user">Card holder</InputLabel>
                      <Input
                        value={cardHolder}
                        id="user"
                        name="user"
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel htmlFor="title">Title</InputLabel>
                      <Input
                        onChange={handleInputChange}
                        value={cardData.title}
                        id="title"
                        name="title"
                        inputProps={{
                          maxLength: '28',
                        }}
                      />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel htmlFor="provider">Provider</InputLabel>
                      <Select
                        value={cardData.provider}
                        onChange={handleInputChange}
                        input={<Input name="provider" id="provider" />}
                      >
                        {providersList.map(name => (
                          <MenuItemStyled key={name} value={name}>
                            {name}
                          </MenuItemStyled>
                        ))}
                      </Select>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel htmlFor="type">Type</InputLabel>
                      <Select
                        value={cardData.type}
                        onChange={handleInputChange}
                        input={<Input name="type" id="type" />}
                      >
                        {typesList.map(name => (
                          <MenuItemStyled key={name} value={name}>
                            {name}
                          </MenuItemStyled>
                        ))}
                      </Select>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <InputLabel htmlFor="currency">Currency</InputLabel>
                      <Select
                        value={cardData.currency}
                        onChange={handleInputChange}
                        input={<Input name="currency" id="currency" />}
                      >
                        {currenciesList.map(name => (
                          <MenuItemStyled key={name} value={name}>
                            {name}
                          </MenuItemStyled>
                        ))}
                      </Select>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                      <div className="picker">
                        <InlineDatePicker
                          views={['year', 'month']}
                          openTo="year"
                          fullWidth
                          format="MMMM YYYY"
                          label="Expiry date"
                          value={cardData.expired_at}
                          onChange={handleExpireChange}
                        />
                      </div>
                    </FormControl>
                  </FormGroupStyled>
                  <Button type="submit" color="primary" variant="contained">
                    <i className="material-icons inner-icons">save</i>
                        Save changes
                  </Button>
                </FormStyled>
              </Grid>
              <Grid item xs={12} sm={12} md={5} lg={5}>
                <LoyaltyCard card={cardData} />
              </Grid>
            </Grid>
          </PaperView>
        </Grid>
        <Grid item xs={12} sm={12} md={11}>
          <PaperHeader>
            <Typography color="inherit" variant="h5" inline>Edit rewards</Typography>
          </PaperHeader>
          <PaperView>
            <Grid container direction="row" justify="center" alignItems="center">
              <Grid item xs={12} sm={12} md={12} lg={10}>
                {card.rewards.map(reward => (
                  <RewardEditable
                    key={reward.id}
                    processReward={updateReward}
                    deleteReward={() => removeRewardDispatch(reward.id)}
                    reward={reward}
                  />
                ))}
                {addRewardOpened ? (
                  <RewardEditable processReward={createReward} />
                ) : (
                  <Button onClick={showAddReward} variant="contained" color="primary">
                    <i className="material-icons inner-icons">add_circle</i>
                    Add reward
                  </Button>
                )}
              </Grid>
            </Grid>
          </PaperView>
        </Grid>
        <SnackbarStyled onClose={handleSnackbarClose} message="Saved!" variant="success" open={snackbarOpened} />
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

EditCard.propTypes = {
  fetchCustomersDispatch: PropTypes.func.isRequired,
  fetchCardDispatch: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  editCardDispatch: PropTypes.func.isRequired,
  addRewardDispatch: PropTypes.func.isRequired,
  editRewardDispatch: PropTypes.func.isRequired,
  removeRewardDispatch: PropTypes.func.isRequired,
  customers: PropTypes.arrayOf(USER_SHAPE).isRequired,
  owner: USER_SHAPE.isRequired,
  card: CARD_SHAPE.isRequired,
};

const mapStateToProps = state => ({
  customers: getCustomers(state),
  owner: getAuthenticatedUser(state),
  card: getCard(state),
});

const mapDispatchToProps = {
  fetchCustomersDispatch: fetchCustomers,
  fetchCardDispatch: fetchCard,
  editCardDispatch: editCard,
  editRewardDispatch: editReward,
  addRewardDispatch: addReward,
  removeRewardDispatch: removeReward,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCard);
