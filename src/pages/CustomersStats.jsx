import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableFooter,
  TablePagination,
} from '@material-ui/core';
import { connect } from 'react-redux';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import { isEmpty } from 'lodash';
import classNames from 'classnames';
import PaperHeader from '../components/Paper/PaperHeader';
import { getCustomersPurchases, getCustomersStatistics, getIsSortAborted } from '../selectors/purchases';
import PaperView from '../components/Paper/PaperView';
import CellStyled from '../components/Table/CellStyled';
import PurchaseTableItem from '../components/Purchases/PurchaseTableItem';
import useFilter from '../hooks/useFilter';
import useSorted from '../hooks/useSorted';
import { fetchCustomers } from '../actions/customersActions';
import { getCustomers } from '../selectors/customers';
import PurchasesFilter from '../components/Purchases/PurchasesFilter';
import {
  fetchCustomersStatistics,
  sortCustomersPurchases,
  filterCustomersPurchases,
} from '../actions/purchasesActions';
import { STATISTICS_SHAPE, PURCHASE_SHAPE, USER_SHAPE } from '../constants';

const CustomersStats = ({
  purchases,
  isSortAborted,
  fetchCustomersDispatch,
  customers,
  fetchCustomersStatisticsDispatch,
  sortCustomersPurchasesDispatch,
  filterCustomersPurchasesDispatch,
  statistics,
}) => {
  const [sort, sortDirectionState] = useSorted(sortCustomersPurchasesDispatch, isSortAborted);
  const [filter, handleFilterChange] = useFilter({
    customers_id: [], card_id: '', amount: '', created_at: '1990-01-01',
  }, filterCustomersPurchasesDispatch);

  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [customersList, setCustomersList] = useState([]);

  const [paginatedPurchases, setPaginatedPurchases] = useState([]);

  useEffect(() => {
    if (isEmpty(customers)) {
      fetchCustomersDispatch();
    } else {
      setCustomersList(customers.map(c => ({ id: c.id, name: c.firstName })));
    }
  }, [customers]);

  useEffect(() => {
    fetchCustomersStatisticsDispatch();
  }, []);

  useEffect(() => {
    const offset = rowsPerPage * currentPage;
    setPaginatedPurchases(purchases.slice(offset, offset + rowsPerPage));
  }, [currentPage, rowsPerPage, purchases]);

  const handleChangeRowsPerPage = ({ target }) => {
    setCurrentPage(0);
    setRowsPerPage(parseInt(target.value, 10));
  };

  const handleChangePage = (event, page) => {
    setCurrentPage(page);
  };


  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Grid container justify="center" alignItems="center">
        <Grid item xs={12} sm={12} md={11}>
          <PaperHeader>
            <Typography color="inherit" variant="h5" inline>Customers' statistics</Typography>
          </PaperHeader>
          <PurchasesFilter
            handleFilterChange={handleFilterChange}
            filter={filter}
            customersList={customersList}
            statistics={statistics}
          />
          <PaperView>
            <Table className="table-md">
              <TableHead>
                <TableRow>
                  <CellStyled align="center" onClick={() => sort('user_id')} className={classNames('sorted', sortDirectionState.user_id)}>
                    Customer ID
                  </CellStyled>
                  <CellStyled align="center" onClick={() => sort('card_id')} className={classNames('sorted', sortDirectionState.card_id)}>
                    Card ID
                  </CellStyled>
                  <CellStyled align="center" onClick={() => sort('amount')} className={classNames('sorted', sortDirectionState.amount)}>
                    Amount
                  </CellStyled>
                  <CellStyled align="center" onClick={() => sort('created_at')} className={classNames('sorted', sortDirectionState.created_at)}>
                    Created
                  </CellStyled>
                </TableRow>
              </TableHead>
              <TableBody>
                {paginatedPurchases.map(purchase => (
                  <PurchaseTableItem forOwner key={purchase.id} purchase={purchase} />
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10]}
                    colSpan={3}
                    count={purchases.length}
                    rowsPerPage={rowsPerPage}
                    page={currentPage}
                    SelectProps={{
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </PaperView>
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

CustomersStats.propTypes = {
  purchases: PropTypes.arrayOf(PURCHASE_SHAPE).isRequired,
  fetchCustomersDispatch: PropTypes.func.isRequired,
  customers: PropTypes.arrayOf(USER_SHAPE).isRequired,
  fetchCustomersStatisticsDispatch: PropTypes.func.isRequired,
  filterCustomersPurchasesDispatch: PropTypes.func.isRequired,
  sortCustomersPurchasesDispatch: PropTypes.func.isRequired,
  isSortAborted: PropTypes.bool.isRequired,
  statistics: STATISTICS_SHAPE.isRequired,
};

const mapStateToProps = state => ({
  purchases: getCustomersPurchases(state),
  customers: getCustomers(state),
  statistics: getCustomersStatistics(state),
  isSortAborted: getIsSortAborted(state),
});

const mapDispatchToProps = {
  fetchCustomersDispatch: fetchCustomers,
  fetchCustomersStatisticsDispatch: fetchCustomersStatistics,
  sortCustomersPurchasesDispatch: sortCustomersPurchases,
  filterCustomersPurchasesDispatch: filterCustomersPurchases,
};


export default connect(mapStateToProps, mapDispatchToProps)(CustomersStats);
