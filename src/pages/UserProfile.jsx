import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import FormGroupStyled from '../components/Form/FormGroupStyled';
import PaperFlex from '../components/Paper/PaperFlex';
import PaperHeader from '../components/Paper/PaperHeader';
import styles from '../assets/jss/userProfile';
import { updateUser } from '../actions/authActions';
import Applications from '../components/UserApplications/Applications';
import FormStyled from '../components/Form/FormStyled';
import { getAuthenticatedUser, getIsOwner } from '../selectors/auth';
import PaperView from '../components/Paper/PaperView';
import SnackbarStyled from '../components/Snackbar/SnackbarStyled';
import { USER_SHAPE } from '../constants';

const UserProfile = ({
  classes, user, updateUserDispatch, isOwner,
}) => {
  const [userData, setUserData] = useState({
    id: '',
    email: '',
    firstName: '',
    lastName: '',
  });
  const [snackbarOpened, setSnackbarOpened] = useState(false);

  const handleSnackbarClose = () => {
    setSnackbarOpened(false);
  };


  const onSubmit = (e) => {
    e.preventDefault();
    updateUserDispatch(userData).then(() => setSnackbarOpened(true));
  };

  useEffect(() => {
    if (user) {
      const {
        id, email, firstName, lastName,
      } = user;
      if (id !== userData.id) {
        setUserData({
          id, email, firstName, lastName,
        });
      }
    }
  }, [user]);

  const handleInputChange = ({ target: { name, value } }) => {
    const newState = {
      [name]: value,
    };
    setUserData({ ...userData, ...newState });
  };

  return (
    <Grid container justify="center" alignItems="center">
      <Grid item xs={12} sm={12} md={11}>
        <PaperHeader>
          <Typography color="inherit" variant="h5">
              Edit profile
          </Typography>
        </PaperHeader>
        <PaperView>
          <Grid container direction="row">
            <Grid item xs={12} sm={12} md={7} lg={7}>
              <FormStyled onSubmit={onSubmit}>
                <FormGroupStyled>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="email">Email Address</InputLabel>
                    <Input
                      onChange={handleInputChange}
                      value={userData.email}
                      id="email"
                      name="email"
                      autoComplete="email"
                    />
                  </FormControl>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="firstName">First name</InputLabel>
                    <Input
                      onChange={handleInputChange}
                      value={userData.firstName}
                      inputProps={{
                        pattern: '[A-Za-z\\s]+',
                      }}
                      id="firstName"
                      name="firstName"
                    />
                  </FormControl>
                  <FormControl margin="normal" fullWidth>
                    <InputLabel htmlFor="lastName">Last name</InputLabel>
                    <Input
                      onChange={handleInputChange}
                      value={userData.lastName}
                      inputProps={{
                        pattern: '[A-Za-z\\s]+',
                      }}
                      id="lastName"
                      name="lastName"
                    />
                  </FormControl>
                </FormGroupStyled>
                <Button type="submit" color="primary" variant="contained">
                  <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                      Save changes
                </Button>
              </FormStyled>
            </Grid>
          </Grid>
        </PaperView>
        {isOwner && (
        <PaperFlex>
          <Grid container>
            <Grid item xs={12} sm={12} md={12}>
              <Applications applications={user.applications} />
            </Grid>
          </Grid>
        </PaperFlex>
        )}
      </Grid>
      <SnackbarStyled onClose={handleSnackbarClose} message="Saved!" variant="success" open={snackbarOpened} />
    </Grid>
  );
};

UserProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  isOwner: PropTypes.bool.isRequired,
  user: USER_SHAPE.isRequired,
  updateUserDispatch: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
  user: getAuthenticatedUser(state),
  isOwner: getIsOwner(state),
});

const mapDispatchToProps = {
  updateUserDispatch: updateUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserProfile));
