import React, { useState, useEffect } from 'react';
import {
  FormControl,
  Grid,
  Input,
  InputLabel,
  Button,
  Typography,
  Select,
} from '@material-ui/core';
import MomentUtils from '@date-io/moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { InlineDatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import moment from 'moment';
import { Redirect } from 'react-router-dom';
import PaperHeader from '../components/Paper/PaperHeader';
import PaperView from '../components/Paper/PaperView';
import { fetchCustomers } from '../actions/customersActions';
import LoyaltyCard from '../components/LoyaltyCards/LoyaltyCard';
import { addCard } from '../actions/cardsActions';
import { CUSTOMER_CARDS } from '../routes';
import MenuItemStyled from '../components/MenuItem/MenuItemStyled';
import FormStyled from '../components/Form/FormStyled';
import FormGroupStyled from '../components/Form/FormGroupStyled';
import { getAuthenticatedUser } from '../selectors/auth';
import { getCustomers } from '../selectors/customers';
import { USER_SHAPE } from '../constants';

const AddCard = ({
  fetchCustomersDispatch, addCardDispatch, customers, owner,
}) => {
  const [makeRedirect, setMakeRedirect] = useState(false);
  const [redirectUrl, setRedirectUrl] = useState('/');
  const [cardData, setCardData] = useState({
    title: '',
    provider: '',
    user_id: '',
    type: '',
    currency: '',
    expired_at: moment(),
  });

  const [customersList, setCustomersList] = useState([]);
  const [providersList, setProvidersList] = useState([]);

  useEffect(() => {
    if (isEmpty(customers)) {
      fetchCustomersDispatch();
    } else {
      setCustomersList(customers.map(c => ({ id: c.id, name: c.firstName })));
      setProvidersList(owner.applications.map(a => a.title));
    }
  }, [customers]);

  const onSubmit = (e) => {
    e.preventDefault();
    addCardDispatch(cardData);
    setRedirectUrl(CUSTOMER_CARDS.replace(':id', cardData.user_id));
    setMakeRedirect(true);
  };

  const handleInputChange = ({ target: { name, value } }) => {
    setCardData({ ...cardData, [name]: value });
  };

  const handleExpireChange = (value) => {
    setCardData({ ...cardData, expired_at: value });
  };

  const typesList = [
    'Savings',
  ];

  const currenciesList = [
    'Dollars',
    'Stars',
    'Cats',
    'Balls',
  ];

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div>
        {makeRedirect && (
          <Redirect to={redirectUrl} />
        )}
        <Grid container justify="center" alignItems="center">
          <Grid item xs={12} sm={12} md={11}>
            <PaperHeader>
              <Typography color="inherit" variant="h5">
              Add card
              </Typography>
            </PaperHeader>
            <PaperView>
              <Grid container direction="row">
                <Grid item xs={12} sm={12} md={7} lg={7}>
                  <FormStyled onSubmit={onSubmit}>
                    <FormGroupStyled>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="user_id">Card holder</InputLabel>
                        <Select
                          value={cardData.user_id}
                          onChange={handleInputChange}
                          input={<Input name="user_id" id="user_id" />}
                        >
                          {customersList.map(c => (
                            <MenuItemStyled key={c.id} value={c.id}>
                              {c.name}
                            </MenuItemStyled>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="title">Title</InputLabel>
                        <Input
                          onChange={handleInputChange}
                          value={cardData.title}
                          id="title"
                          name="title"
                          inputProps={{
                            maxLength: '28',
                          }}
                        />
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="provider">Provider</InputLabel>
                        <Select
                          value={cardData.provider}
                          onChange={handleInputChange}
                          input={<Input name="provider" id="provider" />}
                        >
                          {providersList.map(name => (
                            <MenuItemStyled key={name} value={name}>
                              {name}
                            </MenuItemStyled>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="type">Type</InputLabel>
                        <Select
                          value={cardData.type}
                          onChange={handleInputChange}
                          input={<Input name="type" id="type" />}
                        >
                          {typesList.map(name => (
                            <MenuItemStyled key={name} value={name}>
                              {name}
                            </MenuItemStyled>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="currency">Currency</InputLabel>
                        <Select
                          value={cardData.currency}
                          onChange={handleInputChange}
                          input={<Input name="currency" id="currency" />}
                        >
                          {currenciesList.map(name => (
                            <MenuItemStyled key={name} value={name}>
                              {name}
                            </MenuItemStyled>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <div className="picker">
                          <InlineDatePicker
                            views={['year', 'month']}
                            openTo="year"
                            fullWidth
                            format="MMMM YYYY"
                            label="Expiry date"
                            value={cardData.expired_at}
                            onChange={handleExpireChange}
                          />
                        </div>
                      </FormControl>
                    </FormGroupStyled>
                    <Button type="submit" color="primary" variant="contained">
                      <i className="material-icons inner-icons">save</i>
                        Save changes
                    </Button>
                  </FormStyled>
                </Grid>
                <Grid item xs={12} sm={12} md={5} lg={5}>
                  <LoyaltyCard card={cardData} />
                </Grid>
              </Grid>
            </PaperView>
          </Grid>
        </Grid>
      </div>
    </MuiPickersUtilsProvider>
  );
};

AddCard.propTypes = {
  fetchCustomersDispatch: PropTypes.func.isRequired,
  addCardDispatch: PropTypes.func.isRequired,
  customers: PropTypes.arrayOf(USER_SHAPE).isRequired,
  owner: USER_SHAPE.isRequired,
};

const mapStateToProps = state => ({
  customers: getCustomers(state),
  owner: getAuthenticatedUser(state),
});

const mapDispatchToProps = {
  fetchCustomersDispatch: fetchCustomers,
  addCardDispatch: addCard,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCard);
