import React, { useEffect, useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import PaperHeader from '../components/Paper/PaperHeader';
import { fetchCustomer } from '../actions/customersActions';
import LoyaltyCardsView from '../components/LoyaltyCards/LoyaltyCardsView';
import {
  removeCard, fetchCustomerCards,
} from '../actions/cardsActions';
import { addPurchase } from '../actions/purchasesActions';
import { getCustomer } from '../selectors/customers';
import { getCustomerCards } from '../selectors/cards';
import { CARD_SHAPE, USER_SHAPE } from '../constants';
import PurchaseDialog from '../components/Purchases/PurchaseDialog';

const CustomerCards = ({
  match: { params },
  customer,
  fetchCustomerDispatch,
  fetchCustomerCardsDispatch,
  removeCardDispatch,
  addPurchaseDispatch,
  cards,
}) => {
  const [isPurchaseModalOpened, setPurchaseModalOpened] = useState(false);
  const [purchaseAmount, setPurchaseAmount] = useState(0);
  const [cardForPurchase, setCardForPurchase] = useState({});
  const [reloadCards, setReloadCards] = useState(false);

  useEffect(() => {
    const customerId = parseInt(params.id, 10);
    if (isEmpty(customer) || customer.id !== customerId) {
      fetchCustomerDispatch(customerId);
    } else {
      fetchCustomerCardsDispatch(customerId);
    }
  }, [customer, reloadCards]);

  const submitPurchase = (e) => {
    e.preventDefault();
    addPurchaseDispatch(cardForPurchase.id, parseInt(purchaseAmount, 10))
      .then(() => setReloadCards(!reloadCards));
    setPurchaseModalOpened(false);
  };

  const openPurchaseModal = (card) => {
    setCardForPurchase(card);
    setPurchaseModalOpened(true);
  };

  const closePurchaseModal = () => {
    setPurchaseModalOpened(false);
  };

  return (
    <Grid container justify="center" alignItems="center">
      <Grid item xs={12} sm={12} md={11}>
        <PaperHeader>
          <Typography color="inherit" variant="h5">
            {customer.firstName ? `${customer.firstName}'s cards` : 'Customer\'s cards'}
          </Typography>
        </PaperHeader>
        {!isEmpty(cards) && (
          <LoyaltyCardsView
            openPurchaseModal={openPurchaseModal}
            removeCard={removeCardDispatch}
            isEditable
            cards={cards}
          />
        )}
      </Grid>
      <PurchaseDialog
        cardForPurchase={cardForPurchase}
        submitPurchase={submitPurchase}
        closePurchaseModal={closePurchaseModal}
        isPurchaseModalOpened={isPurchaseModalOpened}
        setPurchaseAmount={setPurchaseAmount}
        purchaseAmount={purchaseAmount}
      />
    </Grid>
  );
};

CustomerCards.propTypes = {
  customer: USER_SHAPE.isRequired,
  fetchCustomerDispatch: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  cards: PropTypes.arrayOf(CARD_SHAPE).isRequired,
  removeCardDispatch: PropTypes.func.isRequired,
  addPurchaseDispatch: PropTypes.func.isRequired,
  fetchCustomerCardsDispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  customer: getCustomer(state),
  cards: getCustomerCards(state),
});

const mapDispatchToProps = {
  fetchCustomerDispatch: fetchCustomer,
  removeCardDispatch: removeCard,
  addPurchaseDispatch: addPurchase,
  fetchCustomerCardsDispatch: fetchCustomerCards,
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerCards);
