import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Typography,
  InputLabel,
  Select,
  Input,
  Radio,
  FormControlLabel,
} from '@material-ui/core';
import { connect } from 'react-redux';
import PaperHeader from '../components/Paper/PaperHeader';
import LoyaltyCardsTable from '../components/LoyaltyCards/LoyaltyCardsTable';
import { filterCards, fetchClientCards, sortCards } from '../actions/cardsActions';
import LoyaltyCardsView from '../components/LoyaltyCards/LoyaltyCardsView';
import MenuItemStyled from '../components/MenuItem/MenuItemStyled';
import RadioGroupInline from '../components/Form/RadioGroupInline';
import PaperFilter from '../components/Paper/PaperFilter';
import FormControlFilter from '../components/Form/FormControlFilter';
import { getIsSortAborted, getClientCards } from '../selectors/cards';
import useFilter from '../hooks/useFilter';
import FilterTitle from '../components/Utils/FilterTitle';
import { CARD_SHAPE } from '../constants';

const CardsList = ({
  fetchClientCardsDispatch, filterCardsDispatch, cards, isSortAborted, sortCardsDispatch,
}) => {
  const [view, setView] = useState('card');
  const handleViewChange = ({ target }) => {
    setView(target.value);
  };

  const [filter, handleFilterChange] = useFilter(
    { provider: [], currency: [] }, filterCardsDispatch,
  );

  useEffect(() => {
    fetchClientCardsDispatch();
  }, []);

  const providers = [
    'Adidas',
    'Nike',
    'Puma',
    'Reebok',
  ];

  const currencies = [
    'Dollars',
    'Stars',
    'Cats',
    'Balls',
  ];

  return (
    <Grid container justify="center" alignItems="center">
      <Grid item xs={12} sm={12} md={11}>
        <PaperHeader>
          <Typography color="inherit" variant="h5" inline>Cards list</Typography>
        </PaperHeader>
        <PaperFilter>
          <Grid container>
            <Grid item xs={12} sm={12} md={12}>
              <FilterTitle variant="h6">Filter</FilterTitle>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormControlFilter>
                <InputLabel htmlFor="provider">Provider</InputLabel>
                <Select
                  multiple
                  value={filter.provider}
                  onChange={handleFilterChange}
                  input={<Input name="provider" id="provider" />}
                >
                  {providers.map(name => (
                    <MenuItemStyled key={name} value={name}>
                      {name}
                    </MenuItemStyled>
                  ))}
                </Select>
              </FormControlFilter>
              <FormControlFilter>
                <InputLabel htmlFor="currency">Currency</InputLabel>
                <Select
                  multiple
                  value={filter.currency}
                  onChange={handleFilterChange}
                  input={<Input name="currency" id="currency" />}
                >
                  {currencies.map(name => (
                    <MenuItemStyled key={name} value={name}>
                      {name}
                    </MenuItemStyled>
                  ))}
                </Select>
              </FormControlFilter>
              <RadioGroupInline
                aria-label="view"
                name="view"
                value={view}
                onChange={handleViewChange}
              >
                <FormControlLabel value="table" control={<Radio />} label="Table" />
                <FormControlLabel value="card" control={<Radio />} label="Card" />
              </RadioGroupInline>
            </Grid>
          </Grid>
        </PaperFilter>
        {view === 'table' && (
          <LoyaltyCardsTable
            sortCardsDispatch={sortCardsDispatch}
            isSortAborted={isSortAborted}
            cards={cards}
          />
        )}
        {view === 'card' && (
          <LoyaltyCardsView cards={cards} />
        )}
      </Grid>
    </Grid>
  );
};

CardsList.propTypes = {
  filterCardsDispatch: PropTypes.func.isRequired,
  fetchClientCardsDispatch: PropTypes.func.isRequired,
  sortCardsDispatch: PropTypes.func.isRequired,
  isSortAborted: PropTypes.bool.isRequired,
  cards: PropTypes.arrayOf(CARD_SHAPE).isRequired,
};

const mapStateToProps = state => ({
  cards: getClientCards(state),
  isSortAborted: getIsSortAborted(state),
});

const mapDispatchToProps = {
  filterCardsDispatch: filterCards,
  fetchClientCardsDispatch: fetchClientCards,
  sortCardsDispatch: sortCards,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardsList);
