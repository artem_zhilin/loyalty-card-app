import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
} from '@material-ui/core';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import PaperHeader from '../components/Paper/PaperHeader';
import { fetchClientPurchases, sortClientPurchases, filterClientPurchases } from '../actions/purchasesActions';
import { getClientPurchases, getIsSortAborted } from '../selectors/purchases';
import PaperView from '../components/Paper/PaperView';
import CellStyled from '../components/Table/CellStyled';
import useSorted from '../hooks/useSorted';
import useFilter from '../hooks/useFilter';
import PurchaseTableItem from '../components/Purchases/PurchaseTableItem';
import PurchasesFilter from '../components/Purchases/PurchasesFilter';
import { PURCHASE_SHAPE } from '../constants';

const CardsActivity = ({
  purchases,
  fetchClientPurchasesDispatch,
  sortClientPurchasesDispatch,
  filterClientPurchasesDispatch,
  isSortAborted,
}) => {
  const [sort, sortDirectionState] = useSorted(sortClientPurchasesDispatch, isSortAborted);
  const [filter, handleFilterChange] = useFilter({ card_id: '', amount: '', created_at: '1990-01-01' }, filterClientPurchasesDispatch);

  useEffect(() => {
    fetchClientPurchasesDispatch();
  }, []);

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Grid container justify="center" alignItems="center">
        <Grid item xs={12} sm={12} md={11}>
          <PaperHeader>
            <Typography color="inherit" variant="h5" inline>Cards activity</Typography>
          </PaperHeader>
          <PurchasesFilter handleFilterChange={handleFilterChange} filter={filter} />
          <PaperView>
            <Table className="table-md">
              <TableHead>
                <TableRow>
                  <CellStyled align="center" onClick={() => sort('card_id')} className={classNames('sorted', sortDirectionState.card_id)}>
                    Card ID
                  </CellStyled>
                  <CellStyled align="center" onClick={() => sort('amount')} className={classNames('sorted', sortDirectionState.amount)}>
                    Amount
                  </CellStyled>
                  <CellStyled align="center" onClick={() => sort('created_at')} className={classNames('sorted', sortDirectionState.created_at)}>
                    Created
                  </CellStyled>
                </TableRow>
              </TableHead>
              <TableBody>
                {purchases.map(purchase => (
                  <PurchaseTableItem key={purchase.id} purchase={purchase} />
                ))}
              </TableBody>
            </Table>
          </PaperView>
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

CardsActivity.propTypes = {
  purchases: PropTypes.arrayOf(PURCHASE_SHAPE).isRequired,
  fetchClientPurchasesDispatch: PropTypes.func.isRequired,
  sortClientPurchasesDispatch: PropTypes.func.isRequired,
  filterClientPurchasesDispatch: PropTypes.func.isRequired,
  isSortAborted: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  purchases: getClientPurchases(state),
  isSortAborted: getIsSortAborted(state),
});

const mapDispatchToProps = {
  fetchClientPurchasesDispatch: fetchClientPurchases,
  sortClientPurchasesDispatch: sortClientPurchases,
  filterClientPurchasesDispatch: filterClientPurchases,
};


export default connect(mapStateToProps, mapDispatchToProps)(CardsActivity);
