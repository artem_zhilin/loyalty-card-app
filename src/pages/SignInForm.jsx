import React, { useState } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import { login } from '../actions/authActions';
import { DASHBOARD } from '../routes';
import styles from '../assets/jss/signInForm';

const SignInForm = ({ loginDispatch, classes, location }) => {
  const [authData, setAuthData] = useState({ email: '', password: '' });
  const [authError, setAuthError] = useState(false);
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = authData;
    if (email && password) {
      loginDispatch(email, password).then((result) => {
        if (result) {
          setRedirectToReferrer(true);
        } else {
          setAuthError(true);
        }
      });
    }
  };

  const handleInputChange = ({ target: { name, value } }) => {
    setAuthData({ ...authData, [name]: value });
  };

  const { from } = location.state || DASHBOARD;
  const {
    email, password,
  } = authData;

  if (redirectToReferrer) return <Redirect to={from} />;

  return (
    <Paper className={classes.paper}>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography className={classes.signInTitle} component="h1" variant="h5">
          Sign in to Loyalty Card App
      </Typography>
      {authError && (
        <p className={classes.error}>
          Invalid email or password
        </p>
      )}
      <form className={classes.form} onSubmit={onSubmit}>
        <FormControl error={authError} margin="normal" required fullWidth>
          <InputLabel htmlFor="email">Email Address</InputLabel>
          <Input
            onChange={handleInputChange}
            value={email}
            id="email"
            name="email"
            autoComplete="email"
            type="email"
            autoFocus
          />
        </FormControl>
        <FormControl error={authError} margin="normal" required fullWidth>
          <InputLabel htmlFor="password">Password</InputLabel>
          <Input
            value={password}
            onChange={handleInputChange}
            name="password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
        </FormControl>
        <Button className={classes.submit} type="submit" fullWidth variant="contained" color="primary">
            Sign in
        </Button>
      </form>
    </Paper>
  );
};

SignInForm.propTypes = {
  loginDispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = {
  loginDispatch: login,
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(SignInForm));
