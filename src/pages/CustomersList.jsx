import React, { useEffect } from 'react';
import {
  Typography,
  Grid,
  Table,
  TableHead,
  TableRow,
  TableBody,
  InputLabel,
  Input,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import CellStyled from '../components/Table/CellStyled';
import PaperHeader from '../components/Paper/PaperHeader';
import PaperView from '../components/Paper/PaperView';
import { fetchCustomers, sortCustomers, filterCustomers } from '../actions/customersActions';
import useSorted from '../hooks/useSorted';
import CustomerTableItem from '../components/Customers/CustomerTableItem';
import PaperFilter from '../components/Paper/PaperFilter';
import FormControlFilter from '../components/Form/FormControlFilter';
import { getCustomers, getIsSortAborted } from '../selectors/customers';
import useFilter from '../hooks/useFilter';
import { USER_SHAPE } from '../constants';
import FilterTitle from '../components/Utils/FilterTitle';

const CustomersList = ({
  fetchCustomersDispatch, sortCustomersDispatch, customers, filterCustomersDispatch, isSortAborted,
}) => {
  const [sort, sortDirectionState] = useSorted(sortCustomersDispatch, isSortAborted);
  const [filter, handleFilterChange] = useFilter({ email: '', firstName: '' }, filterCustomersDispatch);

  useEffect(() => {
    fetchCustomersDispatch();
  }, []);

  return (
    <Grid container justify="center" alignItems="center">
      <Grid item xs={12} sm={12} md={11}>
        <PaperHeader>
          <Typography color="inherit" variant="h5" inline>Customers list</Typography>
        </PaperHeader>
        <PaperFilter>
          <Grid container>
            <Grid item xs={12} sm={12} md={12}>
              <FilterTitle variant="h6">Filter</FilterTitle>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <FormControlFilter>
                <InputLabel htmlFor="email">Email</InputLabel>
                <Input onChange={handleFilterChange} autoComplete="off" name="email" id="email" value={filter.email} />
              </FormControlFilter>
              <FormControlFilter>
                <InputLabel htmlFor="firstName">First name</InputLabel>
                <Input onChange={handleFilterChange} autoComplete="off" name="firstName" id="firstName" value={filter.firstName} />
              </FormControlFilter>
            </Grid>
          </Grid>
        </PaperFilter>
        <PaperView>
          <Table className="table-md">
            <TableHead>
              <TableRow>
                <CellStyled align="right" onClick={() => sort('email')} className={classNames('sorted', sortDirectionState.email)}>
                  Email
                </CellStyled>
                <CellStyled align="right" onClick={() => sort('firstName')} className={classNames('sorted', sortDirectionState.firstName)}>
                  First name
                </CellStyled>
                <CellStyled align="right" onClick={() => sort('lastName')} className={classNames('sorted', sortDirectionState.lastName)}>
                  Last name
                </CellStyled>
                <CellStyled>{' '}</CellStyled>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.map(customer => (
                <CustomerTableItem key={customer.id} customer={customer} />
              ))}
            </TableBody>
          </Table>
        </PaperView>
      </Grid>
    </Grid>
  );
};

CustomersList.propTypes = {
  fetchCustomersDispatch: PropTypes.func.isRequired,
  sortCustomersDispatch: PropTypes.func.isRequired,
  filterCustomersDispatch: PropTypes.func.isRequired,
  customers: PropTypes.arrayOf(USER_SHAPE).isRequired,
  isSortAborted: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  customers: getCustomers(state),
  isSortAborted: getIsSortAborted(state),
});

const mapDispatchToProps = {
  fetchCustomersDispatch: fetchCustomers,
  sortCustomersDispatch: sortCustomers,
  filterCustomersDispatch: filterCustomers,
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomersList);
