import uuidv1 from 'uuid/v1';
import moment from 'moment';
import {
  concat, orderBy, includes, isEmpty, uniqBy,
} from 'lodash';
import {
  FETCH_CLIENT_PURCHASES,
  SORT_CLIENT_PURCHASES,
  FILTER_CLIENT_PURCHASES,
  FETCH_CUSTOMERS_STATISTICS,
  FETCH_CUSTOMERS_PURCHASES,
  SORT_CUSTOMERS_PURCHASES,
  FILTER_CUSTOMERS_PURCHASES,
} from '../types/purchases';
import {
  fetchClientCards, fetchCard, editCard, fetchAllCards,
} from './cardsActions';
import {
  getClientPurchases,
  getInitialClientPurchases,
  getCustomersPurchases,
  getInitialCustomersPurchases,
} from '../selectors/purchases';
import { getAuthenticatedUser } from '../selectors/auth';

export const fetchClientPurchases = () => (dispatch) => {
  dispatch(fetchClientCards()).then((cards) => {
    dispatch({
      type: FETCH_CLIENT_PURCHASES,
      payload: concat(...cards.map(c => c.purchases)),
    });
  });
};

export const fetchCustomersStatistics = () => (dispatch, getState) => {
  const owner = getAuthenticatedUser(getState());
  const ownerApplications = owner.applications.map(a => a.title);
  const statistics = { customersCount: 0, cardsCount: 0, purchasesCount: 0 };
  dispatch(fetchAllCards()).then((cards) => {
    const customersCards = cards.filter(c => includes(ownerApplications, c.provider));
    statistics.cardsCount = customersCards.length;
    statistics.customersCount = uniqBy(customersCards, 'user_id').length;
    const purchases = concat(...customersCards.map(c => c.purchases));
    statistics.purchasesCount = purchases.length;
    dispatch({
      type: FETCH_CUSTOMERS_STATISTICS,
      payload: statistics,
    });
    dispatch({
      type: FETCH_CUSTOMERS_PURCHASES,
      payload: purchases,
    });
  });
};

export const addPurchase = (cardId, purchaseAmount) => dispatch => (
  dispatch(fetchCard(cardId)).then((card) => {
    const cardToEdit = { ...card };
    purchaseAmount = parseInt(purchaseAmount, 10);
    const newPurchase = {
      id: uuidv1(),
      amount: purchaseAmount,
      created_at: moment(),
      card_id: cardId,
      user_id: card.user_id,
    };
    cardToEdit.amount += purchaseAmount;
    cardToEdit.rewards = cardToEdit.rewards.map((reward) => {
      reward.is_achieved = reward.value <= cardToEdit.amount;
      return reward;
    });
    cardToEdit.purchases = [...cardToEdit.purchases, newPurchase];
    dispatch(editCard(cardToEdit));
  })
);

export const sortClientPurchases = (field, direction) => (dispatch, getState) => {
  dispatch({
    type: SORT_CLIENT_PURCHASES,
    payload: orderBy(getClientPurchases(getState()), [field], [direction]),
  });
};

export const filterClientPurchases = conditions => (dispatch, getState) => {
  let purchases = getInitialClientPurchases(getState());
  if (!isEmpty(conditions.card_id)) {
    purchases = purchases.filter(p => includes(p.card_id, conditions.card_id));
  }
  if (!isEmpty(conditions.amount)) {
    purchases = purchases.filter(p => parseInt(conditions.amount, 10) <= p.amount);
  }
  if (!isEmpty(conditions.created_at)) {
    purchases = purchases.filter(p => p.created_at.isSameOrAfter(conditions.created_at));
  }
  dispatch({
    type: FILTER_CLIENT_PURCHASES,
    payload: purchases,
  });
};

export const sortCustomersPurchases = (field, direction) => (dispatch, getState) => {
  dispatch({
    type: SORT_CUSTOMERS_PURCHASES,
    payload: orderBy(getCustomersPurchases(getState()), [field], [direction]),
  });
};

export const filterCustomersPurchases = conditions => (dispatch, getState) => {
  let purchases = getInitialCustomersPurchases(getState());
  if (!isEmpty(conditions.customers_id)) {
    purchases = purchases.filter(p => includes(conditions.customers_id, p.user_id));
  }
  if (!isEmpty(conditions.card_id)) {
    purchases = purchases.filter(p => includes(p.card_id, conditions.card_id));
  }
  if (!isEmpty(conditions.amount)) {
    purchases = purchases.filter(p => parseInt(conditions.amount, 10) <= p.amount);
  }
  if (!isEmpty(conditions.created_at)) {
    purchases = purchases.filter(p => p.created_at.isSameOrAfter(conditions.created_at));
  }
  dispatch({
    type: FILTER_CUSTOMERS_PURCHASES,
    payload: purchases,
  });
};
