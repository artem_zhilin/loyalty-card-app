import axios from 'axios';
import { DO_AUTH, DO_LOGOUT, UPDATE_USER } from '../types/auth';

export const login = (email, password) => dispatch => (
  axios.get('/remote-api/users.json').then((res) => {
    const { data: { users } } = res;
    const user = users.find(item => (item.email === email && item.password === password));
    if (user) {
      delete user.password;
      dispatch({
        type: DO_AUTH,
        payload: user,
      });
    }
    return !!user;
  })
);

export const updateUser = user => (dispatch) => {
  dispatch({
    type: UPDATE_USER,
    payload: user,
  });
  return Promise.resolve();
};

export const logout = () => dispatch => (
  dispatch({
    type: DO_LOGOUT,
  })
);
