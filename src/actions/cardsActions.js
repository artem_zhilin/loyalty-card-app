import axios from 'axios';
import moment from 'moment';
import { orderBy, includes, isEmpty } from 'lodash';
import uuidv1 from 'uuid/v1';
import {
  FETCH_CLIENT_CARDS,
  SORT_CARDS,
  FILTER_CARDS,
  REMOVE_CARD,
  FETCH_ALL_CARDS,
  ADD_CARD,
  EDIT_CARD,
  FETCH_CARD,
  FETCH_CUSTOMER_CARDS,
} from '../types/cards';
import { getInitialCards, getCard, getClientCards } from '../selectors/cards';
import { getAuthenticatedUser } from '../selectors/auth';

export const fetchAllCards = () => (dispatch, getState) => {
  const initialCards = getInitialCards(getState());
  if (isEmpty(initialCards)) {
    return axios.get('/remote-api/cards.json', {
      responseType: 'text',
      transformResponse: res => (
        JSON.parse(res, (key, value) => (key === 'created_at' || key === 'expired_at' ? moment(value) : value))
      ),
    }).then(({ data: { cards } }) => {
      dispatch({
        type: FETCH_ALL_CARDS,
        payload: cards,
      });
      return cards;
    });
  }
  return Promise.resolve(initialCards);
};

export const fetchClientCards = () => (dispatch, getState) => {
  const initialCards = getInitialCards(getState());
  const user = getAuthenticatedUser(getState());
  let clientCards = [];
  if (initialCards.length > 0) {
    clientCards = initialCards.filter(card => card.user_id === user.id);
    dispatch({
      type: FETCH_CLIENT_CARDS,
      payload: clientCards,
    });
    return Promise.resolve(clientCards);
  }
  return dispatch(fetchAllCards()).then((cards) => {
    clientCards = cards.filter(card => card.user_id === user.id);
    dispatch({
      type: FETCH_CLIENT_CARDS,
      payload: clientCards,
    });
    return clientCards;
  });
};

export const fetchCustomerCards = customerId => (dispatch, getState) => {
  const initialCards = getInitialCards(getState());
  const owner = getAuthenticatedUser(getState());
  const ownerApplications = owner.applications.map(a => a.title);
  if (initialCards.length > 0) {
    dispatch({
      type: FETCH_CUSTOMER_CARDS,
      payload: initialCards.filter(c => (
        c.user_id === customerId && includes(ownerApplications, c.provider)
      )),
    });
  } else {
    dispatch(fetchAllCards()).then(cards => (
      dispatch({
        type: FETCH_CUSTOMER_CARDS,
        payload: cards.filter(c => (
          c.user_id === customerId && includes(ownerApplications, c.provider)
        )),
      })
    ));
  }
};

export const sortCards = (field, direction) => (dispatch, getState) => {
  dispatch({
    type: SORT_CARDS,
    payload: orderBy(getClientCards(getState()), [field], [direction]),
  });
};

export const filterCards = conditions => (dispatch, getState) => {
  const initialCards = getInitialCards(getState());
  let cards = initialCards.filter(c => c.user_id === getAuthenticatedUser(getState()).id);
  Object.keys(conditions).map((field) => {
    if (conditions[field].length) {
      cards = cards.filter(card => includes(conditions[field], card[field]));
    }
    return false;
  });
  dispatch({
    type: FILTER_CARDS,
    payload: cards,
  });
};

export const addCard = cardData => (dispatch) => {
  const newCard = {
    ...cardData,
    id: uuidv1(),
    amount: 0,
    created_at: moment(),
    rewards: [],
    purchases: [],
  };
  dispatch({
    type: ADD_CARD,
    payload: newCard,
  });
};

export const fetchCard = cardId => (dispatch, getState) => {
  const initialCards = getInitialCards(getState());
  let card = {};
  if (initialCards.length > 0) {
    card = initialCards.find(c => c.id === cardId);
    dispatch({
      type: FETCH_CARD,
      payload: card,
    });
    return Promise.resolve(card);
  }
  return dispatch(fetchAllCards()).then((cards) => {
    card = cards.find(c => c.id === cardId);
    dispatch({
      type: FETCH_CARD,
      payload: card,
    });
    return card;
  });
};

export const editCard = cardData => (dispatch) => {
  dispatch({
    type: EDIT_CARD,
    payload: cardData,
  });
  return Promise.resolve();
};

export const removeCard = cardId => (dispatch) => {
  dispatch({
    type: REMOVE_CARD,
    payload: cardId,
  });
};

export const addReward = rewardData => (dispatch, getState) => {
  const card = getCard(getState());
  const newReward = { ...rewardData, id: uuidv1() };
  newReward.value = parseInt(newReward.value, 10);
  newReward.is_achieved = newReward.value <= card.amount;
  card.rewards = orderBy([...card.rewards, newReward], ['value']);
  dispatch({
    type: EDIT_CARD,
    payload: card,
  });
};

export const editReward = rewardData => (dispatch, getState) => {
  const card = getCard(getState());
  const oldReward = card.rewards.find(r => r.id === rewardData.id);
  if (oldReward) {
    const newReward = { ...oldReward, ...rewardData };
    newReward.value = parseInt(newReward.value, 10);
    newReward.is_achieved = newReward.value <= card.amount;
    card.rewards = orderBy([...card.rewards.filter(r => r.id !== rewardData.id), newReward], ['value']);
    dispatch({
      type: EDIT_CARD,
      payload: card,
    });
  }
};

export const removeReward = rewardId => (dispatch, getState) => {
  const card = getCard(getState());
  card.rewards = card.rewards.filter(r => r.id !== rewardId);
  dispatch({
    type: EDIT_CARD,
    payload: card,
  });
};
