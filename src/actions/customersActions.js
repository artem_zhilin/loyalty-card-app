import axios from 'axios';
import { orderBy, includes } from 'lodash';
import {
  FETCH_CUSTOMERS, SORT_CUSTOMERS, FETCH_CUSTOMER, FILTER_CUSTOMERS,
} from '../types/customers';
import { getAuthenticatedUser } from '../selectors/auth';
import { getCustomers, getInitialCustomers } from '../selectors/customers';

export const fetchCustomers = () => (dispatch, getState) => (
  axios.get('/remote-api/users.json').then((res) => {
    const { data: { users } } = res;
    const currentUserId = getAuthenticatedUser(getState()).id;
    dispatch({
      type: FETCH_CUSTOMERS,
      payload: users.filter(user => user.id !== currentUserId),
    });
  })
);

export const sortCustomers = (field, direction) => (dispatch, getState) => {
  const customers = getCustomers(getState());
  dispatch({
    type: SORT_CUSTOMERS,
    payload: orderBy(customers, [field], [direction]),
  });
};

export const fetchCustomer = customerId => dispatch => (
  axios.get('/remote-api/users.json').then(({ data: { users } }) => {
    const customer = users.find(u => u.id === customerId);
    dispatch({
      type: FETCH_CUSTOMER,
      payload: customer,
    });
  })
);

export const filterCustomers = conditions => (dispatch, getState) => {
  const initialCustomers = getInitialCustomers(getState());
  let customers = initialCustomers.filter(c => c.id !== getAuthenticatedUser(getState()).id);
  Object.keys(conditions).map((field) => {
    if (!conditions[field].length) {
      return 0;
    }
    customers = customers.filter(customer => includes(customer[field], conditions[field]));
    return 0;
  });
  dispatch({
    type: FILTER_CUSTOMERS,
    payload: customers,
  });
};
