import { useState } from 'react';

const useFilter = (defaultFilterState, filterFunction) => {
  const [filter, setFilter] = useState(defaultFilterState);

  const handleFilterChange = ({ target: { name, value } }) => {
    const newFilter = { ...filter, [name]: value };
    setFilter(newFilter);
    filterFunction(newFilter);
  };

  return [filter, handleFilterChange];
};

export default useFilter;
