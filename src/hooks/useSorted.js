import { useState, useEffect } from 'react';

const useSorted = (sortFunction, isSortAborted) => {
  const [sortDirectionState, setSortDirection] = useState({});

  useEffect(() => {
    if (isSortAborted) {
      setSortDirection({});
    }
  }, [isSortAborted]);

  const sort = (field) => {
    let direction = sortDirectionState[field];
    direction = direction === 'asc' ? 'desc' : 'asc';
    setSortDirection({
      [field]: direction,
    });
    sortFunction(field, direction);
  };

  return [sort, sortDirectionState];
};

export default useSorted;
