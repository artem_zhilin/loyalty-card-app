import DrawerLayout from '../layout/DrawerLayout';
import UserProfile from '../pages/UserProfile';
import CustomerCards from '../pages/CustomerCards';
import AddCard from '../pages/AddCard';
import EditCard from '../pages/EditCard';
import CardsActivity from '../pages/CardsActivity';
import CustomersList from '../pages/CustomersList';
import CardsList from '../pages/CardsList';
import CustomersStats from '../pages/CustomersStats';

export const DASHBOARD = '/';
export const CUSTOMER_LIST = '/customers';
export const CARDS_LIST = '/cards';
export const SIGN_IN = '/sign-in';
export const USER_PROFILE = '/profile';
export const CUSTOMER_CARDS = '/customer/:id/cards';
export const ADD_CARD = '/cards/add';
export const EDIT_CARD = '/cards/:id';
export const CUSTOMERS_STATS = '/customers-stats';

export const PrivateRoutes = [
  {
    title: 'Cards activity',
    path: DASHBOARD,
    layout: DrawerLayout,
    forOwner: false,
    exact: true,
    component: CardsActivity,
    isLink: true,
    icon: 'card_membership',
  },
  {
    title: 'Your cards',
    path: CARDS_LIST,
    layout: DrawerLayout,
    forOwner: false,
    exact: true,
    component: CardsList,
    isLink: true,
    icon: 'card_giftcard',
  },
  {
    title: 'Customers\' statistics',
    path: CUSTOMERS_STATS,
    layout: DrawerLayout,
    forOwner: true,
    exact: true,
    component: CustomersStats,
    icon: 'list_alt',
    isLink: true,
  },
  {
    title: 'Customers list',
    path: CUSTOMER_LIST,
    layout: DrawerLayout,
    forOwner: true,
    exact: true,
    component: CustomersList,
    isLink: true,
    icon: 'person',
  },
  {
    title: 'Add card',
    path: ADD_CARD,
    layout: DrawerLayout,
    forOwner: true,
    exact: true,
    component: AddCard,
    isLink: true,
    icon: 'add_circle',
  },
  {
    title: 'Customer cards',
    path: CUSTOMER_CARDS,
    layout: DrawerLayout,
    forOwner: true,
    exact: true,
    component: CustomerCards,
  },
  {
    title: 'Edit card',
    path: EDIT_CARD,
    layout: DrawerLayout,
    forOwner: true,
    exact: true,
    component: EditCard,
  },
  {
    title: 'Profile',
    path: USER_PROFILE,
    layout: DrawerLayout,
    forOwner: false,
    exact: true,
    component: UserProfile,
  },
];
