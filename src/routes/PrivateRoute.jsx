import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { SIGN_IN, DASHBOARD } from '.';
import { getIsOwner, getIsAuthenticated } from '../selectors/auth';

const PrivateRoute = ({
  isAuthenticated, isOwner, forOwner, layout: Layout, component: Component, ...rest
}) => {
  const getRenderValue = (props) => {
    let renderValue = (
      <Redirect to={{
        pathname: SIGN_IN,
        state: { from: props.location },
      }}
      />
    );
    if (isAuthenticated) {
      if (!forOwner || (forOwner && isOwner)) {
        renderValue = <Layout><Component {...props} /></Layout>;
      } else if (forOwner && !isOwner) {
        renderValue = <Redirect to={DASHBOARD} />;
      }
    }
    return renderValue;
  };
  return <Route {...rest} render={getRenderValue} />;
};

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isOwner: PropTypes.bool,
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(React.Component),
  ]).isRequired,
  layout: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(React.Component),
  ]).isRequired,
  forOwner: PropTypes.bool,
};

PrivateRoute.defaultProps = {
  forOwner: false,
  isOwner: false,
};

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isOwner: getIsOwner(state),
});

export default connect(mapStateToProps, {})(PrivateRoute);
