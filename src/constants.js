import PropTypes from 'prop-types';

export const CARD_TABLE_FIELDS = ['title', 'provider', 'type', 'currency', 'amount', 'created_at', 'expired_at'];
export const USER_SHAPE = PropTypes.shape({
  id: PropTypes.number,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  applications: PropTypes.arrayOf(PropTypes.object),
});
export const CARD_SHAPE = PropTypes.shape({
  id: PropTypes.string,
  title: PropTypes.string,
  provider: PropTypes.string,
  type: PropTypes.string,
  currency: PropTypes.string,
  amount: PropTypes.number,
  rewards: PropTypes.arrayOf(PropTypes.object),
  created_at: PropTypes.object,
  expired_at: PropTypes.object,
});
export const STATISTICS_SHAPE = PropTypes.shape({
  customersCount: PropTypes.number,
  cardsCount: PropTypes.number,
  purchasesCount: PropTypes.number,
});
export const PURCHASE_SHAPE = PropTypes.shape({
  id: PropTypes.string,
  card_id: PropTypes.string,
  amount: PropTypes.number,
  created_at: PropTypes.object,
  user_id: PropTypes.number,
});
export const REWARD_SHAPE = PropTypes.shape({
  id: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  is_achieved: PropTypes.bool,
});
