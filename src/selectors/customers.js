export const getCustomers = ({ customers }) => customers.customers;

export const getCustomer = ({ customers }) => customers.customer;

export const getInitialCustomers = ({ customers }) => customers.initialCustomers;

export const getIsSortAborted = ({ customers }) => customers.isSortAborted;
