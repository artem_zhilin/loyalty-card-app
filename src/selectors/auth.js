import { createSelector } from 'reselect';
import { isEmpty } from 'lodash';

export const getToken = ({ auth }) => auth.token;

export const getIsAuthenticated = ({ auth }) => auth.isAuthenticated;

export const getAuthenticatedUser = ({ auth }) => auth.user;

export const getIsOwner = createSelector(
  getAuthenticatedUser,
  user => !isEmpty(user.applications),
);
