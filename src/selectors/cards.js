import { orderBy } from 'lodash';

export const getClientCards = ({ cards }) => cards.clientCards;

export const getCard = ({ cards }) => cards.card;

export const getIsSortAborted = ({ cards }) => cards.isSortAborted;

export const getInitialCards = ({ cards }) => cards.initialCards;

export const getCustomerCards = ({ cards }) => orderBy(cards.customerCards, ['id']);
