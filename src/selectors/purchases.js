export const getClientPurchases = ({ purchases }) => purchases.clientFilteredPurchases;

export const getInitialClientPurchases = ({ purchases }) => purchases.clientPurchases;

export const getIsSortAborted = ({ purchases }) => purchases.isSortAborted;

export const getCustomersPurchases = ({ purchases }) => purchases.customersFilteredPurchases;

export const getInitialCustomersPurchases = ({ purchases }) => purchases.customersPurchases;

export const getCustomersStatistics = ({ purchases }) => purchases.customersStatistics;
