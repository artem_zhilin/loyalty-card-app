import React from 'react';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import * as PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import {
  DASHBOARD,
  USER_PROFILE,
} from '../../routes';
import styles from '../../assets/jss/navbar';


const Navbar = ({
  classes, logoutDispatch, handleDrawerToggle,
}) => (
  <AppBar position="fixed" className={classes.appBar}>
    <Toolbar>
      {handleDrawerToggle && (
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
      )}
      <Grid container alignItems="center" justify="space-between" direction="row">
        <Grid item>
          <Link to={DASHBOARD}>
            <i className={`${classes.logoIcon} material-icons`}>card_giftcard</i>
            <Typography variant="h6" color="inherit" className={classes.toolbarTitle} noWrap>
                    Loyalty Card App
            </Typography>
          </Link>
        </Grid>
        <Grid item>
          <Link to={USER_PROFILE}>
            <Button color="inherit" className="btnOutlined">
              <i className="material-icons inner-icons">account_circle</i>
              <div className={classes.btnText}>Profile</div>
            </Button>
          </Link>
          <Button color="inherit" onClick={logoutDispatch} className={`${classes.btnSignOut} btnOutlined`}>
            <i className="material-icons inner-icons">exit_to_app</i>
            <div className={classes.btnText}>Sign out</div>
          </Button>
        </Grid>
      </Grid>
    </Toolbar>
  </AppBar>
);

Navbar.propTypes = {
  classes: PropTypes.object.isRequired,
  logoutDispatch: PropTypes.func.isRequired,
  handleDrawerToggle: PropTypes.func,
};

Navbar.defaultProps = {
  handleDrawerToggle: undefined,
};

export default withStyles(styles)(Navbar);
