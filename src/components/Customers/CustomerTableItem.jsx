import React from 'react';
import { TableRow, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import CellStyled from '../Table/CellStyled';
import { CUSTOMER_CARDS } from '../../routes';
import { USER_SHAPE } from '../../constants';

const CustomerTableItem = ({ customer }) => (
  <TableRow>
    <CellStyled align="right">{customer.email}</CellStyled>
    <CellStyled align="right">{customer.firstName}</CellStyled>
    <CellStyled align="right">{customer.lastName}</CellStyled>
    <CellStyled align="right">
      <Link to={CUSTOMER_CARDS.replace(':id', customer.id)}>
        <Button variant="outlined" color="primary">Manage cards</Button>
      </Link>
    </CellStyled>
  </TableRow>
);

CustomerTableItem.propTypes = {
  customer: USER_SHAPE.isRequired,
};

export default CustomerTableItem;
