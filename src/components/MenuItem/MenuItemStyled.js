import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';

const MenuItemStyled = withStyles(theme => ({
  root: {
    padding: theme.spacing.unit * 1.25,
    height: theme.spacing.unit * 2,
  },
}))(MenuItem);

export default MenuItemStyled;
