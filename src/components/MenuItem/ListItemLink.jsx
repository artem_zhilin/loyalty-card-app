import { NavLink } from 'react-router-dom';
import React from 'react';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  icon: {
    margin: 0,
  },
  normalLink: {
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.2)',
    },
  },
  activeLink: {
    backgroundColor: 'rgba(0,0,0,0.12)',
  },
};

const ListItemLink = ({
  icon, primary, secondary, to, classes,
}) => {
  const renderLink = itemProps => (
    <NavLink activeClassName={classes.activeLink} exact to={to} {...itemProps} />
  );

  return (
    <li>
      <ListItem classes={{ button: classes.normalLink }} button component={renderLink}>
        {icon && (
        <ListItemIcon className={classes.icon}>
          <i className="material-icons">{icon}</i>
        </ListItemIcon>
        )}
        <ListItemText inset primary={primary} secondary={secondary} />
      </ListItem>
    </li>
  );
};

ListItemLink.propTypes = {
  icon: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  primary: PropTypes.string.isRequired,
  secondary: PropTypes.string,
  to: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};

ListItemLink.defaultProps = {
  icon: false,
  secondary: '',
};

export default withStyles(styles)(ListItemLink);
