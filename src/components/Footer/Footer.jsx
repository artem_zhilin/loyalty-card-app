import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import styles from '../../assets/jss/footer';

const Footer = ({ classes }) => (
  <Fragment>
    <footer className={classes.footer}>
      <Typography className={classes.ptxs} variant="subtitle1" color="inherit" align="center" component="p">
          Something here to give the footer a purpose!
      </Typography>
    </footer>
  </Fragment>
);

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
