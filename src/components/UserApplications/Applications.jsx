import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import PropTypes from 'prop-types';
import PaperHeader from '../Paper/PaperHeader';

const Applications = ({ applications }) => (
  <Fragment>
    <PaperHeader>
      <Typography color="inherit" variant="h5">
        Applications
      </Typography>
    </PaperHeader>
    <List>
      {applications.map(app => (
        <Fragment key={app.id}>
          <ListItem>
            <ListItemText>
              {app.title}
              {' - '}
              {app.address}
            </ListItemText>
          </ListItem>
          <Divider />
        </Fragment>
      ))}
    </List>
  </Fragment>
);

Applications.propTypes = {
  applications: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      address: PropTypes.string,
    }),
  ).isRequired,
};

export default Applications;
