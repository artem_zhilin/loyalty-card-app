import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

const CellStyled = withStyles(theme => ({
  root: {
    fontSize: '0.9em',
    padding: `${theme.spacing.unit * 0.5}px ${theme.spacing.unit * 2.5}px ${theme.spacing.unit * 0.5}px ${theme.spacing.unit * 3}px`,
  },
}))(TableCell);

export default CellStyled;
