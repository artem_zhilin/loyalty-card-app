import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Input, InputLabel, Button, Fab, Divider,
} from '@material-ui/core';
import { pick } from 'lodash';
import FormStyled from '../Form/FormStyled';
import TooltipStyled from '../Tooltip/TooltipStyled';
import FormControlFilter from '../Form/FormControlFilter';
import { REWARD_SHAPE } from '../../constants';


const RewardEditable = ({ reward, processReward, deleteReward }) => {
  const [rewardData, setRewardData] = useState({
    id: undefined,
    title: '',
    value: '',
  });
  useEffect(() => {
    setRewardData(pick(reward, ['id', 'title', 'value']));
  }, []);

  const handleInputChange = ({ target: { name, value } }) => {
    setRewardData({ ...rewardData, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    processReward(rewardData);
  };

  return (
    <React.Fragment>
      <FormStyled onSubmit={onSubmit}>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid className="text-center" item xs={12} sm={5} md={4} lg={4}>
            <FormControlFilter required>
              <InputLabel htmlFor="title">Title</InputLabel>
              <Input
                onChange={handleInputChange}
                value={rewardData.title}
                id="title"
                name="title"
              />
            </FormControlFilter>
          </Grid>
          <Grid className="text-center" item xs={12} sm={5} md={4} lg={4}>
            <FormControlFilter required>
              <InputLabel htmlFor="value">Value</InputLabel>
              <Input
                onChange={handleInputChange}
                value={rewardData.value}
                id="value"
                name="value"
                type="number"
              />
            </FormControlFilter>
          </Grid>
          <Grid className="text-center m-b" item xs={12} sm={2} md={1} lg={1}>
            {reward.is_achieved ? (
              <TooltipStyled title="Achieved" placement="top">
                <Fab color="primary">
                  <i className="material-icons icon-md">check</i>
                </Fab>
              </TooltipStyled>
            ) : (
              <TooltipStyled title="Not achieved yet" placement="top">
                <Fab color="secondary">
                  <i className="material-icons icon-md">hourglass_empty</i>
                </Fab>
              </TooltipStyled>
            )}
          </Grid>
          <Grid className="text-center m-b" item xs={12} sm={6} md={3} lg={3}>
            <Button className="m-l-sm" type="submit" variant="outlined">
              Save
            </Button>
            {deleteReward !== undefined && (
              <Button className="m-l-sm" onClick={deleteReward} variant="outlined">
                Remove
              </Button>
            )}
          </Grid>
        </Grid>
      </FormStyled>
      <Divider />
    </React.Fragment>
  );
};

RewardEditable.propTypes = {
  reward: REWARD_SHAPE,
  processReward: PropTypes.func.isRequired,
  deleteReward: PropTypes.func,
};

RewardEditable.defaultProps = {
  reward: {
    id: undefined,
    title: '',
    value: 0,
    is_achieved: false,
  },
  deleteReward: undefined,
};

export default RewardEditable;
