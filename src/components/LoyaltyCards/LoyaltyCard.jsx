/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Card, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import injectHtml from 'html-react-parser';
import {
  findLast, isEmpty, isUndefined, upperFirst,
} from 'lodash';
import { Link } from 'react-router-dom';
import moment from 'moment';
import styles from '../../assets/jss/loyaltyCard';
import { EDIT_CARD } from '../../routes';
import CurrencyIcon from './CurrencyIcon';
import { CARD_SHAPE } from '../../constants';

// eslint-disable-next-line react/prop-types
const NextRewardTitle = ({ card, nextReward, lastAchievedReward }) => (
  <div>
    {nextReward ? (
      <Fragment>
        {nextReward.value - card.amount}
        {' '}
        <CurrencyIcon currency={card.currency} />
        {` to ${nextReward.title}`}
      </Fragment>
    ) : (
      !lastAchievedReward && (
        <em>Next reward</em>
      )
    )}
  </div>
);


const LoyaltyCard = ({
  card,
  classes,
  isEditable,
  removeCard,
  openPurchaseModal,
}) => {
  const [lastAchievedRewardTitle, setLastAchievedRewardTitle] = useState('<em>Most recent achieved reward</em>');
  const [lastAchievedReward, setLastAchievedReward] = useState();
  const [nextReward, setNextReward] = useState();

  const fieldOrPlaceholder = field => (
    (!isUndefined(card[field]) && !isEmpty(card[field].toString())) ? (
      card[field]
    ) : (
      <em>{upperFirst(field)}</em>
    )
  );

  useEffect(() => {
    if (!isEmpty(card.rewards)) {
      const lastAchievedRewardItem = findLast(card.rewards, item => item.is_achieved);
      setLastAchievedReward(lastAchievedRewardItem);
      setLastAchievedRewardTitle(
        !isUndefined(lastAchievedRewardItem) ? lastAchievedRewardItem.title : 'No achieved reward yet',
      );
      setNextReward(card.rewards.find(item => !item.is_achieved));
    } else {
      setLastAchievedRewardTitle('<em>Most recent achieved reward</em>');
    }
  }, [card]);

  const removeOnConfirm = () => {
    // eslint-disable-next-line no-undef
    if (window.confirm('Are you sure you want to delete this item?')) {
      removeCard(card.id);
    }
  };

  return (
    <Card className={classes.root}>
      <Typography variant="h6" color="inherit" className={classes.title}>
        {fieldOrPlaceholder('title')}
      </Typography>
      <Typography variant="h6" color="inherit" className={classes.type}>
        {fieldOrPlaceholder('type')}
        {' '}
        <CurrencyIcon currency={card.currency} />
        {': '}
        {fieldOrPlaceholder('amount')}
      </Typography>
      <Typography variant="h6" color="inherit" className={classes.achievedReward}>{injectHtml(lastAchievedRewardTitle)}</Typography>
      <Typography variant="h6" color="inherit" className={classes.nextReward}>
        <NextRewardTitle
          nextReward={nextReward}
          card={card}
          lastAchievedReward={lastAchievedReward}
        />
      </Typography>
      <Typography variant="h6" color="inherit" className={classes.provider}>
        {fieldOrPlaceholder('provider')}
      </Typography>
      <Typography variant="h6" color="inherit" className={classes.expired}>
        {moment.isMoment(card.expired_at) ? (
          `Exp. ${card.expired_at.format('MM/YYYY')}`
        ) : (
          <em>Expiry date</em>
        )}
      </Typography>
      {isEditable && (
        <div className={classes.ownerButtons}>
          {openPurchaseModal !== undefined && (
            <i onClick={() => openPurchaseModal(card)} role="button" tabIndex={0} className="material-icons pointer">
              add_shopping_cart
            </i>
          )}
          <Link to={EDIT_CARD.replace(':id', card.id)}>
            <i className="material-icons">
              edit
            </i>
          </Link>
          {removeCard !== undefined && (
            <i onClick={removeOnConfirm} role="button" tabIndex={0} className="material-icons pointer">
              delete
            </i>
          )}
        </div>
      )}
    </Card>
  );
};

LoyaltyCard.propTypes = {
  card: CARD_SHAPE.isRequired,
  classes: PropTypes.object.isRequired,
  isEditable: PropTypes.bool,
  removeCard: PropTypes.func,
  openPurchaseModal: PropTypes.func,
};

LoyaltyCard.defaultProps = {
  isEditable: false,
  removeCard: undefined,
  openPurchaseModal: undefined,
};

export default withStyles(styles)(LoyaltyCard);
