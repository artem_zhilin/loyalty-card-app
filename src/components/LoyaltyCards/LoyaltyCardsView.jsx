import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import PaperView from '../Paper/PaperView';
import LoyaltyCard from './LoyaltyCard';
import { CARD_SHAPE } from '../../constants';

const LoyaltyCardsView = ({
  cards, isEditable, removeCard, openPurchaseModal,
}) => (
  <Grid container>
    <Grid item xs={12} sm={12} md={12}>
      <PaperView>
        <Grid container alignItems="center" justify="flex-start" direction="row">
          {cards.map(card => (
            <Grid key={card.id} item xs={12} sm={6} md={4} lg={4}>
              <LoyaltyCard
                openPurchaseModal={openPurchaseModal}
                removeCard={removeCard}
                isEditable={isEditable}
                card={card}
              />
            </Grid>
          ))}
        </Grid>
      </PaperView>
    </Grid>
  </Grid>
);

LoyaltyCardsView.propTypes = {
  cards: PropTypes.arrayOf(CARD_SHAPE).isRequired,
  isEditable: PropTypes.bool,
  removeCard: PropTypes.func,
  openPurchaseModal: PropTypes.func,
};

LoyaltyCardsView.defaultProps = {
  isEditable: false,
  removeCard: undefined,
  openPurchaseModal: undefined,
};

export default LoyaltyCardsView;
