import React from 'react';
import * as PropTypes from 'prop-types';
import { upperFirst } from 'lodash';
import {
  Table, TableRow, TableHead, TableBody,
} from '@material-ui/core';
import classNames from 'classnames';
import CellStyled from '../Table/CellStyled';
import PaperView from '../Paper/PaperView';
import useSorted from '../../hooks/useSorted';
import LoyaltyCardTableItem from './LoyaltyCardTableItem';
import { CARD_TABLE_FIELDS, CARD_SHAPE } from '../../constants';

const LoyaltyCardsTable = ({
  sortCardsDispatch, cards, isSortAborted,
}) => {
  const [sort, sortDirectionState] = useSorted(sortCardsDispatch, isSortAborted);
  return (
    <PaperView>
      <Table className="table-md">
        <TableHead>
          <TableRow>
            {CARD_TABLE_FIELDS.map((field, index) => (
              <CellStyled key={index} align="right" onClick={() => sort(field)} className={classNames('sorted', sortDirectionState[field])}>
                {upperFirst(field).replace('_', ' ')}
              </CellStyled>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {cards.map(card => (
            <LoyaltyCardTableItem key={card.id} card={card} />
          ))}
        </TableBody>
      </Table>
    </PaperView>
  );
};

LoyaltyCardsTable.propTypes = {
  sortCardsDispatch: PropTypes.func.isRequired,
  isSortAborted: PropTypes.bool.isRequired,
  cards: PropTypes.arrayOf(CARD_SHAPE).isRequired,
};

export default LoyaltyCardsTable;
