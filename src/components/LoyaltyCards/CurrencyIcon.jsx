import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

const CurrencyIcon = ({ currency }) => {
  const [iconClass, setIconClass] = useState('dollar-sign');

  useEffect(() => {
    switch (currency) {
      case 'Cats':
        setIconClass('cat');
        break;
      case 'Balls':
        setIconClass('futbol');
        break;
      case 'Stars':
        setIconClass('star');
        break;
      default:
        setIconClass('dollar-sign');
    }
  }, [currency]);

  return (
    <i className={`fas fa-${iconClass}`} />
  );
};

CurrencyIcon.propTypes = {
  currency: PropTypes.string,
};

CurrencyIcon.defaultProps = {
  currency: 'Dollars',
};

export default CurrencyIcon;
