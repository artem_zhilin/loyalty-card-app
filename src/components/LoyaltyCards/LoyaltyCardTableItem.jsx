import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
import CellStyled from '../Table/CellStyled';
import { CARD_TABLE_FIELDS, CARD_SHAPE } from '../../constants';

const LoyaltyCardTableItem = ({ card }) => (
  <TableRow>
    {CARD_TABLE_FIELDS.map((field, index) => (
      <CellStyled key={index} align="right">
        {moment.isMoment(card[field]) ? (
          card[field].format('YYYY-MM-DD')
        ) : (
          card[field]
        )}
      </CellStyled>
    ))}
  </TableRow>
);

LoyaltyCardTableItem.propTypes = {
  card: CARD_SHAPE.isRequired,
};

export default LoyaltyCardTableItem;
