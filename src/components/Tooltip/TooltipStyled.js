import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';

const TooltipStyled = withStyles({
  tooltip: {
    fontSize: '1em',
  },
})(Tooltip);

export default TooltipStyled;
