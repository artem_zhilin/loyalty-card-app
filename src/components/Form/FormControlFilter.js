import { FormControl } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const FormControlFilter = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit,
    minWidth: '200px',
  },
}))(FormControl);

export default FormControlFilter;
