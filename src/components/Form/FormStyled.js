import styled from 'styled-components';

const FormStyled = styled.form`
    margin-top: 8px;
    padding: 0 20px 30px 20px;
`;

export default FormStyled;
