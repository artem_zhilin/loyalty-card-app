import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';

const FormGroupStyled = withStyles(theme => ({
  root: {
    marginBottom: theme.spacing.unit * 2,
  },
}))(FormGroup);

export default FormGroupStyled;
