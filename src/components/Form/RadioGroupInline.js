import { withStyles } from '@material-ui/core/styles';
import RadioGroup from '@material-ui/core/RadioGroup';

const RadioGroupInline = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit,
    display: 'inline-block',
  },
}))(RadioGroup);

export default RadioGroupInline;
