import React, { Fragment } from 'react';
import * as PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import {
  PrivateRoutes,
} from '../../routes';
import ListItemLink from '../MenuItem/ListItemLink';

const drawerWidth = '240px';

const styles = theme => ({
  root: {
    [theme.breakpoints.up('lg')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  a: {
    color: '#fff',
  },
  drawerWrapper: {
    height: '100%',
  },
  drawerRoot: {
    height: '100%',
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    minHeight: 'calc(100vh - 40px)',
    position: 'initial',
    width: drawerWidth,
  },
});

const DrawerStyled = ({
  classes, mobileOpen, handleDrawerToggle, isOwner,
}) => {
  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {PrivateRoutes.filter(r => !r.forOwner && r.isLink).map(route => (
          <ListItemLink
            key={route.path}
            to={route.path}
            primary={route.title}
            icon={route.icon}
          />
        ))}
      </List>
      {isOwner && (
        <Fragment>
          <Divider />
          <List>
            {PrivateRoutes.filter(r => r.forOwner && r.isLink).map(route => (
              <ListItemLink
                key={route.path}
                to={route.path}
                primary={route.title}
                icon={route.icon}
              />
            ))}
          </List>
        </Fragment>
      )}
    </div>
  );
  return (
    <nav className={classes.root}>
      <Hidden lgUp implementation="css">
        <Drawer
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
      <Hidden className={classes.drawerWrapper} mdDown implementation="css">
        <Drawer
          classes={{
            root: classes.drawerRoot,
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {drawer}
        </Drawer>
      </Hidden>
    </nav>
  );
};

DrawerStyled.propTypes = {
  classes: PropTypes.object.isRequired,
  handleDrawerToggle: PropTypes.func.isRequired,
  mobileOpen: PropTypes.bool.isRequired,
  isOwner: PropTypes.bool,
};

DrawerStyled.defaultProps = {
  isOwner: false,
};

export default withStyles(styles)(DrawerStyled);
