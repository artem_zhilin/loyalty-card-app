import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const PaperHeader = withStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    padding: theme.spacing.unit * 2.5,
    color: '#ffffff',
    borderRadius: 0,
  },
}))(Paper);

export default PaperHeader;
