import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const PaperFilter = withStyles(theme => ({
  root: {
    padding: `${theme.spacing.unit * 2.5}px ${theme.spacing.unit * 2.5}px ${theme.spacing.unit * 1.25}px ${theme.spacing.unit * 2.5}px`,
    marginTop: theme.spacing.unit * 2.5,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderBottom: '1px solid #0000004f',
  },
}))(Paper);

export default PaperFilter;
