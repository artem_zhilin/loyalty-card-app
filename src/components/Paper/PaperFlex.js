import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const PaperFlex = withStyles(theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}))(Paper);

export default PaperFlex;
