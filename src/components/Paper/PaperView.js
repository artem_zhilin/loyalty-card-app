import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const PaperView = withStyles({
  root: {
    width: '100%',
    marginTop: '0',
    marginBottom: '30px',
    overflowX: 'auto',
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    padding: '20px 0',
  },
})(Paper);

export default PaperView;
