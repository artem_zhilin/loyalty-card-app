import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const ButtonOutlined = withStyles({
  root: {
    border: '1px solid #d9e0538f',
    color: 'inherit',
  },
})(Button);

export default ButtonOutlined;
