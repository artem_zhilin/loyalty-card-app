import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const FilterTitle = withStyles(theme => ({
  root: {
    margin: `0 ${theme.spacing.unit}px`,
  },
}))(Typography);

export default FilterTitle;
