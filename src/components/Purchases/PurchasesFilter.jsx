import React from 'react';
import {
  Grid,
  InputLabel,
  Input,
  Badge,
  Select,
} from '@material-ui/core';
import { InlineDatePicker } from 'material-ui-pickers';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import PaperFilter from '../Paper/PaperFilter';
import FormControlFilter from '../Form/FormControlFilter';
import MenuItemStyled from '../MenuItem/MenuItemStyled';
import TooltipStyled from '../Tooltip/TooltipStyled';
import FilterTitle from '../Utils/FilterTitle';

const PurchasesFilter = ({
  statistics, handleFilterChange, filter, customersList,
}) => (
  <PaperFilter>
    <Grid container>
      <Grid item xs={12} sm={12} md={12}>
        <Grid container justify="space-between">
          <Grid item xs={12} sm={9} md={10} lg={10}>
            <FilterTitle variant="h6">Filter</FilterTitle>
          </Grid>
          {!isEmpty(statistics) && (
          <Grid item xs={12} sm={3} md={2} lg={2}>
            <Grid container justify="space-evenly">
              <TooltipStyled title="Total customers" placement="top">
                <Badge color="primary" badgeContent={statistics.customersCount}>
                  <i className="material-icons">person</i>
                </Badge>
              </TooltipStyled>
              <TooltipStyled title="Total cards" placement="top">
                <Badge color="primary" badgeContent={statistics.cardsCount}>
                  <i className="material-icons">card_membership</i>
                </Badge>
              </TooltipStyled>
              <TooltipStyled title="Total purchases" placement="top">
                <Badge color="primary" badgeContent={statistics.purchasesCount}>
                  <i className="material-icons">shopping_cart</i>
                </Badge>
              </TooltipStyled>
            </Grid>
          </Grid>
          )}
        </Grid>
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        {!isEmpty(customersList) && (
        <FormControlFilter>
          <InputLabel htmlFor="customers_id">Customers</InputLabel>
          <Select
            multiple
            value={filter.customers_id}
            onChange={handleFilterChange}
            input={<Input name="customers_id" id="customers_id" />}
          >
            {customersList.map(c => (
              <MenuItemStyled key={c.id} value={c.id}>
                {c.name}
              </MenuItemStyled>
            ))}
          </Select>
        </FormControlFilter>
        )}
        <FormControlFilter>
          <InputLabel htmlFor="card_id">Card ID</InputLabel>
          <Input
            onChange={handleFilterChange}
            autoComplete="off"
            name="card_id"
            id="card_id"
            value={filter.card_id}
          />
        </FormControlFilter>
        <FormControlFilter>
          <InputLabel htmlFor="amount">Amount from</InputLabel>
          <Input
            onChange={handleFilterChange}
            autoComplete="off"
            name="amount"
            id="amount"
            type="number"
            inputProps={{
              min: 1,
            }}
            value={filter.amount}
          />
        </FormControlFilter>
        <FormControlFilter>
          <div className="picker">
            <InlineDatePicker
              fullWidth
              format="MMM Do YYYY"
              label="Created after"
              value={filter.created_at}
              onChange={value => handleFilterChange({ target: { name: 'created_at', value } })}
            />
          </div>
        </FormControlFilter>
      </Grid>
    </Grid>
  </PaperFilter>
);

PurchasesFilter.propTypes = {
  statistics: PropTypes.shape({
    customersCount: PropTypes.number,
    cardsCount: PropTypes.number,
    purchasesCount: PropTypes.number,
  }),
  handleFilterChange: PropTypes.func.isRequired,
  filter: PropTypes.object.isRequired,
  customersList: PropTypes.arrayOf(PropTypes.object),
};

PurchasesFilter.defaultProps = {
  statistics: {},
  customersList: [],
};

export default PurchasesFilter;
