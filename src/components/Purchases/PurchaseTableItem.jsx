import React from 'react';
import { TableRow } from '@material-ui/core';
import PropTypes from 'prop-types';
import moment from 'moment';
import CellStyled from '../Table/CellStyled';
import { PURCHASE_SHAPE } from '../../constants';

const PurchaseTableItem = ({ purchase, forOwner }) => (
  <TableRow>
    {forOwner && (
      <CellStyled align="center">{purchase.user_id}</CellStyled>
    )}
    <CellStyled align="center">{purchase.card_id}</CellStyled>
    <CellStyled align="center">{purchase.amount}</CellStyled>
    <CellStyled align="center">{moment.isMoment(purchase.created_at) ? purchase.created_at.format('YYYY-MM-DD') : purchase.created_at}</CellStyled>
  </TableRow>
);

PurchaseTableItem.propTypes = {
  purchase: PURCHASE_SHAPE.isRequired,
  forOwner: PropTypes.bool,
};

PurchaseTableItem.defaultProps = {
  forOwner: false,
};

export default PurchaseTableItem;
