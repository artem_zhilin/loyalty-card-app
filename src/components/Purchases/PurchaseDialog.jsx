import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Input,
  InputAdornment,
  Grid,
} from '@material-ui/core';
import { range } from 'lodash';
import PropTypes from 'prop-types';
import FormGroupStyled from '../Form/FormGroupStyled';
import CurrencyIcon from '../LoyaltyCards/CurrencyIcon';
import { CARD_SHAPE } from '../../constants';

const PurchaseDialog = ({
  submitPurchase,
  isPurchaseModalOpened,
  closePurchaseModal,
  cardForPurchase,
  setPurchaseAmount,
  purchaseAmount,
}) => (
  <Dialog
    open={isPurchaseModalOpened}
    onClose={closePurchaseModal}
    aria-labelledby="form-dialog-title"
  >
    <DialogTitle id="form-dialog-title">Purchase</DialogTitle>
    <form onSubmit={submitPurchase}>
      <DialogContent>
        <DialogContentText>
          <span className="block m-b">{`Manual form to add purchase to card ${cardForPurchase.id}`}</span>
          <span>{`Current balance: ${cardForPurchase.amount}`}</span>
          {' '}
          <CurrencyIcon currency={cardForPurchase.currency} />
        </DialogContentText>
        <FormGroupStyled>
          <Input
            autoFocus
            margin="dense"
            id="amount"
            label="Purchase amount"
            type="number"
            value={purchaseAmount}
            onChange={({ target }) => setPurchaseAmount(target.value)}
            fullWidth
            required
            startAdornment={<InputAdornment position="start"><CurrencyIcon currency={cardForPurchase.currency} /></InputAdornment>}
            inputProps={{
              min: 1,
            }}
          />
        </FormGroupStyled>
        <Grid container direction="row" justify="space-evenly">
          {range(100, 600, 100).map(amount => (
            <Button onClick={(() => setPurchaseAmount(amount))} color="primary" size="medium" key={amount} variant="contained">
              {amount}
              {'\u00a0'}
              <CurrencyIcon currency={cardForPurchase.currency} />
            </Button>
          ))}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={closePurchaseModal} color="primary">
              Cancel
        </Button>
        <Button type="submit" color="primary">
              Submit
        </Button>
      </DialogActions>
    </form>
  </Dialog>
);

PurchaseDialog.propTypes = {
  cardForPurchase: CARD_SHAPE.isRequired,
  submitPurchase: PropTypes.func.isRequired,
  isPurchaseModalOpened: PropTypes.bool.isRequired,
  closePurchaseModal: PropTypes.func.isRequired,
  setPurchaseAmount: PropTypes.func.isRequired,
  purchaseAmount: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default PurchaseDialog;
