module.exports = {
    "extends": ["airbnb"],
    "parser": "babel-eslint",
    "plugins": [
        "react",
        "jsx-a11y",
        "import",
        "spellcheck"
      ],
      "rules": {
        "spellcheck/spell-checker": [1,
            {
                "comments": false,
                "strings": true,
                "identifiers": true,
                "lang": "en_US",
                "skipWords": [
                    "bool",
                    "mixins",
                    "reselect",
                    "func",
                    "redux",
                    "nav",
                    "html",
                    "dom",
                    "lodash",
                    "jss",
                    "mui",
                    "classnames",
                    "fas",
                    'auth',
                    "tooltip",
                    "params",
                    "snackbar",
                    "utils",
                    "api",
                    "axios",
                    "asc",
                    "desc",
                    "middleware",
                    "btn",
                    "navbar",
                 ],
                 "skipIfMatch": [
                     "http://[^s]*",
                     "uuid*",
                     "[0-9]+px"
                 ],
                 "minLength": 3
             }
         ]
     }
};